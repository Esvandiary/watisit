#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "watisit/common.h"

#if defined(__cplusplus)
extern "C" {
#endif

#define WATISIT_OS_VERSION_STRING_LENGTH 64U
#define WATISIT_OS_DESCRIPTION_LENGTH    128U
#define WATISIT_OS_COMPONENT_NAME_LENGTH 64U
#define WATISIT_OS_SHORT_NAME_LENGTH     32U

typedef enum wiOS
{
    wiOS_Unknown = 0x0000,
    wiOS_Windows = 0x0100,
    wiOS_Linux   = 0x0200,
    wiOS_MacOS   = 0x0300,
    wiOS_BSD     = 0x0400,
    wiOS_FreeBSD = 0x0401,
    wiOS_OpenBSD = 0x0402,
    wiOS_NetBSD  = 0x0403,
    wiOS_Android = 0x0500
} wiOS;

typedef struct wiOSComponentInfo
{
    char name[WATISIT_OS_COMPONENT_NAME_LENGTH];
    uint32_t version_major;
    uint32_t version_minor;
    uint32_t version_patch;
    uint32_t version_build;
    char version_string[WATISIT_OS_VERSION_STRING_LENGTH];
    bool is_supported;
} wiOSComponentInfo;

typedef struct wiOSInfo
{
    wiOS os;
    char name[WATISIT_OS_SHORT_NAME_LENGTH];

    wiOSComponentInfo kernel;
    wiOSComponentInfo distro;

    char description[WATISIT_OS_DESCRIPTION_LENGTH];
    uint32_t word_size;

    bool is_supported;
} wiOSInfo;

WATISIT_EXPORT bool wiOSPopulateInfo(wiOSInfo* info);

static inline wiOSInfo wiOSGetInfo(void)
{
    wiOSInfo oinfo;
    wiOSPopulateInfo(&oinfo);
    return oinfo;
}

#if defined(__cplusplus)
}
#endif
