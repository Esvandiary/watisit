#pragma once

#include "watisit/common.h"

#include <stdbool.h>
#include <stdint.h>

#if defined(__cplusplus)
extern "C" {
#endif

typedef struct wiCPUDetailsARM
{
    bool dummy;
} wiCPUDetailsARM;

typedef struct wiCPUFeaturesARM
{
    bool NEON;
} wiCPUFeaturesARM;

#if defined(__cplusplus)
}
#endif