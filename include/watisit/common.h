#pragma once

// -----------------
// CPU architectures
// -----------------
#define WATISIT_ARCHID_UNKNOWN 0x00000U
#define WATISIT_ARCHID_X86     0x10000U
#define WATISIT_ARCHID_ARM     0x20000U

#if defined(_M_X64) || defined(__x86_64) || defined(__x86_64__) || defined(__amd64__) || defined(__amd64)
    #define WATISIT_ARCH_X86
    #define WATISIT_ARCH_X86_64
    #define WATISIT_ARCHBITS 64
    #define WATISIT_ARCH_CURRENT WATISIT_ARCHID_X86
#elif defined(_X86_) || defined(_M_IX86) || defined(__i386__) || defined(i386)
    #define WATISIT_ARCH_X86
    #define WATISIT_ARCH_X86_32
    #define WATISIT_ARCHBITS 32U
    #define WATISIT_ARCH_CURRENT WATISIT_ARCHID_X86
#elif defined(__aarch64__) || defined(_M_ARM64)
    #define WATISIT_ARCH_ARM
    #define WATISIT_ARCH_ARM64
    #define WATISIT_ARCHBITS 64U
    #define WATISIT_ARCH_CURRENT WATISIT_ARCHID_ARM
#elif defined(__thumb__) || defined(_M_ARMT)
    #define WATISIT_ARCH_ARM
    #define WATISIT_ARCH_ARM_THUMB
    #define WATISIT_ARCHBITS 32U
    #define WATISIT_ARCH_CURRENT WATISIT_ARCHID_ARM
#elif defined(__arm__) || defined(_M_ARM)
    #define WATISIT_ARCH_ARM
    // TODO: ARMEL vs ARMHF?
    #define WATISIT_ARCHBITS 32U
    #define WATISIT_ARCH_CURRENT WATISIT_ARCHID_ARM
#else
    #define WATISIT_ARCH_UNKNOWN
    #define WATISIT_ARCH_CURRENT WATISIT_ARCHID_UNKNOWN
#endif


// -----------------
// Operating systems
// -----------------
#if defined(__APPLE__)
    #define WATISIT_OS_MACOS
    #define WATISIT_OS_UNIX
#elif defined(__ANDROID__)
    #define WATISIT_OS_ANDROID
    #define WATISIT_OS_UNIX
#elif defined(__linux__)
    #define WATISIT_OS_LINUX
    #define WATISIT_OS_UNIX
#elif defined(_WIN32)
    #define WATISIT_OS_WINDOWS
// TODO: elif FreeBSD, OpenBSD, NetBSD
#else
    #define WATISIT_OS_UNKNOWN
#endif


// ------------------------
// Compilers and toolchains
// ------------------------
#if defined(_MSC_VER)
    #define WATISIT_COMPILER_MSVC _MSC_VER
#elif defined(__INTEL_COMPILER) || defined(__ICC)
    // Currently not explicitly supported
    #define WATISIT_COMPILER_INTEL
#elif defined(__clang__)
    #define WATISIT_COMPILER_CLANG
#elif defined(__GNUC__)
    #define WATISIT_COMPILER_GCC
#elif defined(__llvm__)
    // Currently not explicitly supported
    #define WATISIT_COMPILER_LLVM
// TODO: MinGW?
#else
    #define WATISIT_COMPILER_UNKNOWN
#endif


// -------------------------
// Export/import definitions
// -------------------------
#if defined(WATISIT_EXPORT_SYMBOLS) || defined(WATISIT_IMPORT_SYMBOLS)
    #if defined(WATISIT_OS_WINDOWS)
        #if defined(WATISIT_EXPORT_SYMBOLS)
            #define WATISIT_EXPORT       __declspec(dllexport)
            #define WATISIT_CLASS_EXPORT __declspec(dllexport)
        #else
            #define WATISIT_EXPORT       __declspec(dllimport)
            #define WATISIT_CLASS_EXPORT __declspec(dllimport)
        #endif
    #else
        #define WATISIT_EXPORT       __attribute__((visibility("default")))
        #define WATISIT_CLASS_EXPORT __attribute__((visibility("default")))
    #endif
#endif

#if !defined(WATISIT_EXPORT)
    #define WATISIT_EXPORT
#endif
#if !defined(WATISIT_CLASS_EXPORT)
    #define WATISIT_CLASS_EXPORT
#endif
