#pragma once

#include "watisit/common.h"

#include <stdbool.h>
#include <stdint.h>

#if defined(__cplusplus)
extern "C" {
#endif

typedef enum wiCPUVendorX86
{
    wiCPUVendorX86_Unknown = 0x00000,
    wiCPUVendorX86_Intel   = 0x10000,
    wiCPUVendorX86_AMD,
    wiCPUVendorX86_Cyrix,
    wiCPUVendorX86_Nexgen,
    wiCPUVendorX86_Transmeta,
    wiCPUVendorX86_UMC,
    wiCPUVendorX86_Centaur,
    wiCPUVendorX86_Rise,
    wiCPUVendorX86_SiS,
    wiCPUVendorX86_NSC,
    wiCPUVendorX86_VIA
} wiCPUVendorX86;

typedef struct wiCPUDetailsX86
{
    wiCPUVendorX86 vendor;

    uint32_t family;
    uint32_t model;
    uint32_t stepping;
    uint32_t ext_family;
    uint32_t ext_model;
    uint32_t sse_size;
} wiCPUDetailsX86;

// x86 feature list originally from https://github.com/anrieff/libcpuid
typedef struct wiCPUFeatureFlagsX86
{
    bool FPU;         // Floating point unit
    bool VME;         // Virtual mode extension
    bool DE;          // Debugging extension
    bool PSE;         // Page size extension
    bool TSC;         // Time-stamp counter
    bool MSR;         // Model-specific regsisters; RDMSR/WRMSR supported
    bool PAE;         // Physical address extension
    bool MCE;         // Machine check exception
    bool CX8;         // CMPXCHG8B instruction supported
    bool APIC;        // APIC support
    bool MTRR;        // Memory type range registers
    bool SEP;         // SYSENTER / SYSEXIT instructions supported
    bool PGE;         // Page global enable
    bool MCA;         // Machine check architecture
    bool CMOV;        // CMOVxx instructions supported
    bool PAT;         // Page attribute table
    bool PSE36;       // 36-bit page address extension
    bool PN;          // Processor serial # implemented (Intel P3 only)
    bool CLFLUSH;     // CLFLUSH instruction supported
    bool DTS;         // Debug store supported
    bool ACPI;        // ACPI support (power states)
    bool MMX;         // MMX instruction set supported
    bool FXSR;        // FXSAVE / FXRSTOR supported
    bool SSE;         // Streaming-SIMD Extensions (SSE) supported
    bool SSE2;        // SSE2 instructions supported
    bool SS;          // Self-snoop
    bool HT;          // Hyper-threading supported (but might be disabled)
    bool TM;          // Thermal monitor
    bool IA64;        // IA64 supported (Itanium only)
    bool PBE;         // Pending-break enable
    bool SSE3;        // PNI (SSE3) instructions supported
    bool PCLMUL;      // PCLMULQDQ instruction supported
    bool DTS64;       // 64-bit Debug store supported
    bool MONITOR;     // MONITOR / MWAIT supported
    bool DS_CPL;      // CPL Qualified Debug Store
    bool VMX;         // Virtualization technology supported
    bool SMX;         // Safer mode exceptions
    bool EST;         // Enhanced SpeedStep
    bool TM2;         // Thermal monitor 2
    bool SSSE3;       // SSSE3 instructionss supported (this is different from SSE3!)
    bool CID;         // Context ID supported
    bool CX16;        // CMPXCHG16B instruction supported
    bool XTPR;        // Send Task Priority Messages disable
    bool PDCM;        // Performance capabilities MSR supported
    bool DCA;         // Direct cache access supported
    bool SSE4_1;      // SSE 4.1 instructions supported
    bool SSE4_2;      // SSE 4.2 instructions supported
    bool SYSCALL;     // SYSCALL / SYSRET instructions supported
    bool XD;          // Execute disable bit supported
    bool MOVBE;       // MOVBE instruction supported
    bool POPCNT;      // POPCNT instruction supported
    bool AES;         // AES* instructions supported
    bool XSAVE;       // XSAVE/XRSTOR/etc instructions supported
    bool OSXSAVE;     // non-privileged copy of OSXSAVE supported
    bool AVX;         // Advanced vector extensions supported
    bool MMXEXT;      // AMD MMX-extended instructions supported
    bool THREEDNOW;   // AMD 3DNow! instructions supported
    bool THREEDNOWEXT;// AMD 3DNow! extended instructions supported
    bool NX;          // No-execute bit supported
    bool FXSR_OPT;    // FFXSR: FXSAVE and FXRSTOR optimizations
    bool RDTSCP;      // RDTSCP instruction supported (AMD-only)
    bool LM;          // Long mode (x86_64/EM64T) supported
    bool LAHF_LM;     // LAHF/SAHF supported in 64-bit mode
    bool CMP_LEGACY;  // core multi-processing legacy mode
    bool SVM;         // AMD Secure virtual machine
    bool ABM;         // LZCNT instruction support
    bool MISALIGNSSE; // Misaligned SSE supported
    bool SSE4A;       // SSE 4a from AMD
    bool PREFETCH;    // 3DNow PREFETCH/PREFETCHW support
    bool OSVW;        // OS Visible Workaround (AMD)
    bool IBS;         // Instruction-based sampling
    bool SSE5;        // SSE 5 instructions supported (deprecated; will never be 1)
    bool SKINIT;      // SKINIT / STGI supported
    bool WDT;         // Watchdog timer support
    bool TS;          // Temperature sensor
    bool FID;         // Frequency ID control
    bool VID;         // Voltage ID control
    bool TTP;         // THERMTRIP
    bool TM_AMD;      // AMD-specified hardware thermal control
    bool STC;         // Software thermal control
    bool STEP100MHZ;  // 100 MHz multiplier control
    bool HWPSTATE;    // Hardware P-state control
    bool CONSTANT_TSC;// TSC ticks at constant rate
    bool XOP;         // The XOP instruction set (same as the old CPU_FEATURE_SSE5)
    bool FMA3;        // The FMA3 instruction set
    bool FMA4;        // The FMA4 instruction set
    bool TBM;         // Trailing bit manipulation instruction support
    bool F16C;        // 16-bit FP convert instruction support
    bool RDRAND;      // RdRand instruction
    bool X2APIC;      // x2APIC; APIC_BASE.EXTD; MSRs 0000_0800h...0000_0BFFh 64-bit ICR (+030h but not +031h); no DFR (+00Eh); SELF_IPI (+040h) also see standard level 0000_000Bh
    bool CPB;         // Core performance boost
    bool APERFMPERF;  // MPERF/APERF MSRs support
    bool PFI;         // Processor Feedback Interface support
    bool PA;          // Processor accumulator
    bool AVX2;        // AVX2 instructions
    bool BMI1;        // BMI1 instructions
    bool BMI2;        // BMI2 instructions
    bool HLE;         // Hardware Lock Elision prefixes
    bool RTM;         // Restricted Transactional Memory instructions
    bool AVX512F;     // AVX-512 Foundation
    bool AVX512DQ;    // AVX-512 Double/Quad granular insns
    bool AVX512PF;    // AVX-512 Prefetch
    bool AVX512ER;    // AVX-512 Exponential/Reciprocal
    bool AVX512CD;    // AVX-512 Conflict detection
    bool SHA_NI;      // SHA-1/SHA-256 instructions
    bool AVX512BW;    // AVX-512 Byte/Word granular insns
    bool AVX512VL;    // AVX-512 128/256 vector length extensions
    bool RDSEED;      // RDSEED instruction
    bool ADX;         // ADX extensions (arbitrary precision)
    bool SGX;         // Intel SGX support
    bool SGX1;        // Intel SGX1 extensions
    bool SGX2;        // Intel SGX2 extensions
} wiCPUFeatureFlagsX86;

typedef struct wiCPUFeaturesX86
{
    wiCPUFeatureFlagsX86 flags;

    // Some features are more complex to check support for
    // So, provide a more accurate picture outside the individual CPUID flags
    bool SSE;
    bool SSE2;
    bool SSE3;
    bool SSSE3;
    bool SSE4_1;
    bool SSE4_2;
    bool AVX;
    bool AVX2;
} wiCPUFeaturesX86;

#if defined(__cplusplus)
}
#endif