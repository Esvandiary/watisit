#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include "watisit/common.h"
#include "watisit/cpu_x86.h"
#include "watisit/cpu_arm.h"

#if defined(__cplusplus)
extern "C" {
#endif

// Determine which details and features structs to use
#if defined(WATISIT_ARCH_X86)
    typedef wiCPUDetailsX86  wiCPUDetails;
    typedef wiCPUFeaturesX86 wiCPUFeatures;
#elif defined(WATISIT_ARCH_ARM)
    typedef wiCPUDetailsARM  wiCPUDetails;
    typedef wiCPUFeaturesARM wiCPUFeatures;
#else
    struct wiCPUDetailsUnknown {};
    struct wiCPUFeaturesUnknown {};
    typedef struct wiCPUDetailsUnknown wiCPUDetails;
    typedef struct wiCPUFeaturesUnknown wiCPUFeatures;
#endif

// Set size limits of struct components
#define WATISIT_CPU_VENDOR_NAME_SIZE 64U
#define WATISIT_CPU_DESCRIPTION_SIZE 128U

// Define different architectures
typedef enum wiCPUArch
{
    wiCPUArch_Unknown = WATISIT_ARCHID_UNKNOWN,
    wiCPUArch_x86     = WATISIT_ARCHID_X86,
    wiCPUArch_ARM     = WATISIT_ARCHID_ARM
} wiCPUArch;

// Set current architecture
const wiCPUArch wiCurrentArch = (wiCPUArch)WATISIT_ARCH_CURRENT;


typedef struct wiCPUInfo
{
    wiCPUArch architecture;
    uint32_t word_size;
    uint32_t num_physical_cores;
    uint32_t num_logical_cores;
    char vendor_name[WATISIT_CPU_VENDOR_NAME_SIZE];
    char description[WATISIT_CPU_DESCRIPTION_SIZE];

    wiCPUDetails details;
    wiCPUFeatures features;

    bool is_supported;
} wiCPUInfo;


WATISIT_EXPORT bool wiCPUPopulateInfo(wiCPUInfo* info);

static inline wiCPUInfo wiCPUGetInfo(void)
{
    wiCPUInfo cinfo;
    wiCPUPopulateInfo(&cinfo);
    return cinfo;
}

WATISIT_EXPORT uint32_t wiGetTotalLogicalCPUCount(void);

#if defined(__cplusplus)
}
#endif
