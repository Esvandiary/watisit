#pragma once

#include "watisit/common.h"

#include <stdbool.h>
#include <stdint.h>

#if defined(__cplusplus)
extern "C" {
#endif

typedef struct wiMemoryData
{
    uint64_t limit;
    uint64_t total;
    uint64_t available;
    uint64_t used;
    bool is_supported;
} wiMemoryData;

typedef struct wiMemoryInfo
{
    wiMemoryData phys;
    wiMemoryData swap;
    wiMemoryData virt;

    bool is_supported;
} wiMemoryInfo;

WATISIT_EXPORT bool wiMemoryPopulateInfo(wiMemoryInfo* info);

static inline wiMemoryInfo wiMemoryGetInfo(void)
{
    wiMemoryInfo minfo;
    wiMemoryPopulateInfo(&minfo);
    return minfo;
}

#if defined(__cplusplus)
}
#endif
