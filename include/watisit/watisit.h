#pragma once

#include <stdbool.h>

#include "watisit/common.h"

#include "watisit/cpu.h"
#include "watisit/memory.h"
#include "watisit/os.h"

#if defined(__cplusplus)
extern "C" {
#endif

typedef struct wiMachineInfo
{
    wiCPUInfo    cpu;
    wiMemoryInfo memory;
    wiOSInfo     os;
} wiMachineInfo;

static inline bool wiPopulateInfo(wiMachineInfo* info)
{
    bool result = false;
    result =    wiCPUPopulateInfo(&info->cpu)    || result;
    result = wiMemoryPopulateInfo(&info->memory) || result;
    result =     wiOSPopulateInfo(&info->os)     || result;
    return result;
}

static inline wiMachineInfo wiGetInfo(void)
{
    wiMachineInfo info;
    wiPopulateInfo(&info);
    return info;
}

#if defined(__cplusplus)
}
#endif
