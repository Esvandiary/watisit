#include "watisit/watisit.h"

#include <string>
#include <iostream>
#include <sstream>

std::string get_mem_str(const wiMemoryData& data)
{
    std::ostringstream ss;
    ss << data.used/(1UL<<20) << "/" << data.total/(1UL<<20) << "MB";
    if (data.limit != data.total)
        ss << " (limit: " << data.limit/(1UL<<20) << "MB)";
    return ss.str();
}

int main(int argc, char** argv)
{
    wiMachineInfo info = wiGetInfo();

    const wiCPUInfo& cinfo = info.cpu;
    if (cinfo.is_supported)
    {
        std::cout << "================" << std::endl;
        std::cout << " CPU INFO:   OK " << std::endl;
        std::cout << "================" << std::endl;

        std::cout << "Vendor: " << cinfo.vendor_name << std::endl;
        std::cout << "Description: " << cinfo.description << std::endl;
        std::cout << "Cores: " << cinfo.num_physical_cores << "c/" << cinfo.num_logical_cores << "t" << std::endl;

        std::cout << "SIMD:";
#if defined(WATISIT_ARCH_X86)
        if (cinfo.features.SSE) std::cout << " SSE";
        if (cinfo.features.SSE2) std::cout << " SSE2";
        if (cinfo.features.SSE3) std::cout << " SSE3";
        if (cinfo.features.SSSE3) std::cout << " SSSE3";
        if (cinfo.features.SSE4_1) std::cout << " SSE4.1";
        if (cinfo.features.SSE4_2) std::cout << " SSE4.2";
        if (cinfo.features.AVX) std::cout << " AVX";
        if (cinfo.features.AVX2) std::cout << " AVX2";
        if (cinfo.features.flags.FMA3) std::cout << " FMA3";
        if (cinfo.features.flags.F16C) std::cout << " F16C";
        if (cinfo.features.flags.AVX512F) std::cout << " AVX512F";
        if (cinfo.features.flags.AVX512DQ) std::cout << " AVX512DQ";
        if (cinfo.features.flags.AVX512CD) std::cout << " AVX512CD";
        if (cinfo.features.flags.AVX512PF) std::cout << " AVX512PF";
        if (cinfo.features.flags.AVX512BW) std::cout << " AVX512BW";
        if (cinfo.features.flags.AVX512ER) std::cout << " AVX512ER";
        if (cinfo.features.flags.AVX512VL) std::cout << " AVX512VL";
#elif defined(WATISIT_ARCH_ARM)
        if (cinfo.features.NEON) std::cout << " NEON";
#endif
        std::cout << std::endl;

        std::cout << "Word size: " << cinfo.word_size << std::endl;
#if defined(WATISIT_ARCH_X86)
        std::cout << "Family: " << cinfo.details.family
                  << ", Model: " << cinfo.details.model
                  << ", Stepping: " << cinfo.details.stepping << std::endl;
        std::cout << "ExtFamily: " << cinfo.details.ext_family
                  << ", ExtModel: " << cinfo.details.ext_model
                  << ", SSE size: " << cinfo.details.sse_size << std::endl;
#endif

        size_t total_core_count = wiGetTotalLogicalCPUCount();
        if (total_core_count > 0)
            std::cout << "Total system core count: " << total_core_count << std::endl;
    }
    else
    {
        std::cout << "================" << std::endl;
        std::cout << " CPU INFO: NONE " << std::endl;
        std::cout << "================" << std::endl;
    }
    std::cout << std::endl;

    const wiMemoryInfo& minfo = info.memory;
    if (minfo.is_supported)
    {
        std::cout << "===================" << std::endl;
        std::cout << " MEMORY INFO:   OK " << std::endl;
        std::cout << "===================" << std::endl;
        if (minfo.phys.is_supported)
            std::cout << "Phys: " << get_mem_str(minfo.phys) << std::endl;
        if (minfo.swap.is_supported)
            std::cout << "Swap: " << get_mem_str(minfo.swap) << std::endl;
        if (minfo.virt.is_supported)
            std::cout << "Virt: " << get_mem_str(minfo.virt) << std::endl;
    }
    else
    {
        std::cout << "===================" << std::endl;
        std::cout << " MEMORY INFO: NONE " << std::endl;
        std::cout << "===================" << std::endl;
    }
    std::cout << std::endl;

    const wiOSInfo& oinfo = info.os;
    if (oinfo.is_supported)
    {
        std::cout << "===============" << std::endl;
        std::cout << " OS INFO:   OK " << std::endl;
        std::cout << "===============" << std::endl;
        std::cout << "OS Name: " << oinfo.name << " (" << oinfo.word_size << "-bit)" << std::endl;
        if (oinfo.kernel.is_supported)
        {
            std::cout << "Kernel: " << oinfo.kernel.name << " " << oinfo.kernel.version_string
                      << " (" << oinfo.kernel.name << ", " << oinfo.kernel.version_major
                      << ", " << oinfo.kernel.version_minor << ", " << oinfo.kernel.version_patch
                      << ", " << oinfo.kernel.version_build << ")" << std::endl;
        }
        if (oinfo.distro.is_supported)
        {
            std::cout << "Distro: " << oinfo.distro.name << " " << oinfo.distro.version_string
                      << " (" << oinfo.distro.name << ", " << oinfo.distro.version_major
                      << ", " << oinfo.distro.version_minor << ", " << oinfo.distro.version_patch
                      << ", " << oinfo.distro.version_build << ")" << std::endl;
        }
        std::cout << "Description: " << oinfo.description << std::endl;
    }
    else
    {
        std::cout << "===============" << std::endl;
        std::cout << " OS INFO: NONE " << std::endl;
        std::cout << "===============" << std::endl;
    }
    std::cout << std::endl;
    return 0;
}
