#include "watisit/cpu.h"
#include "watisit/memory.h"
#include "watisit/os.h"

#include <string>
#include <iostream>
#include <iomanip>
#include <chrono>
#include <cstdlib>

double duration_as_us(std::chrono::steady_clock::duration dur, size_t iterations)
{
    const uint64_t usTotal = std::chrono::duration_cast<std::chrono::microseconds>(dur).count();
    return static_cast<double>(usTotal) / iterations;
}

void run(size_t iterations)
{
    uint64_t dummy1 = 0, dummy2 = 0, dummy3 = 0;
    auto start = std::chrono::steady_clock::now();

    for (size_t i = 0; i < iterations; ++i)
    {
        wiCPUInfo cinfo = wiCPUGetInfo();
        dummy1 += cinfo.num_logical_cores;
    }

    auto cpu = std::chrono::steady_clock::now();

    for (size_t i = 0; i < iterations; ++i)
    {
        wiMemoryInfo minfo = wiMemoryGetInfo();
        dummy2 += minfo.phys.used;
    }

    auto mem = std::chrono::steady_clock::now();

    for (size_t i = 0; i < iterations; ++i)
    {
        wiOSInfo oinfo = wiOSGetInfo();
        dummy3 += oinfo.kernel.version_major;
    }

    auto end = std::chrono::steady_clock::now();

    std::cout << "After " << iterations << " iterations:" << std::endl;
    std::cout << std::fixed << std::setfill(' ') << std::setprecision(3);
    std::cout << "  cpu: " << std::setw(7) << duration_as_us(cpu - start, iterations) << std::setw(0) << "us / iteration" << std::endl;
    std::cout << "  mem: " << std::setw(7) << duration_as_us(mem - cpu, iterations) << std::setw(0) << "us / iteration" << std::endl;
    std::cout << "   os: " << std::setw(7) << duration_as_us(end - mem, iterations) << std::setw(0) << "us / iteration" << std::endl;
    std::cout << std::endl;
    std::cout << "dummy values: " << dummy1 << " " << dummy2 << " " << dummy3 << std::endl;
}

int main(int argc, char** argv)
{
    size_t iterations = 50000;
    if (argc > 1)
        iterations = std::atol(argv[1]);

    run(iterations);
    return 0;
}
