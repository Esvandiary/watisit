#include <stdio.h>
#include <inttypes.h>
#include <watisit/watisit.h>

void print_machine_info()
{
    // Obtain machine info
    wiMachineInfo info = wiGetInfo();
    // Print CPU info
    if (info.cpu.is_supported)
    {
        printf("CPU: %s\n", info.cpu.description);
        printf("Cores: %u cores / %u threads\n", info.cpu.num_physical_cores, info.cpu.num_logical_cores);
    }
    // Print memory info
    if (info.memory.phys.is_supported)
        printf("RAM: %" PRIu64 "/%" PRIu64 " MiB used\n", info.memory.phys.used / 1048576, info.memory.phys.total / 1048576);
    if (info.memory.swap.is_supported)
        printf("Swap: %" PRIu64 "/%" PRIu64 " MiB used\n", info.memory.swap.used / 1048576, info.memory.swap.total / 1048576);
    if (info.memory.virt.is_supported)
        printf("VMem: %" PRIu64 "/%" PRIu64 " MiB used\n", info.memory.virt.used / 1048576, info.memory.virt.total / 1048576);
    // Print OS info
    if (info.os.is_supported)
        printf("OS: %s\n", info.os.description);
    if (info.os.kernel.is_supported)
        printf("Kernel: %s %s\n", info.os.kernel.name, info.os.kernel.version_string);
    if (info.os.distro.is_supported)
        printf("Distro: %s %s\n", info.os.distro.name, info.os.distro.version_string);
}

int main(int argc, char** argv)
{
    print_machine_info();
    return 0;
}