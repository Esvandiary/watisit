# watisit - a cross-platform C/C++ library for basic hardware detection

This library allows the detection of basic hardware information (CPU info, RAM usage, OS info) on various operating systems and architectures. This could be useful for any application where information about the runtime hardware environment is valuable - to infer expected performance levels, for example.

## Capabilities

- **CPU info:** model name, vendor, family details, supported features, core count, word size
- **Memory info:** used/free/totals for physical, swap and virtual memory
- **OS info:** description, kernel name and version, distro name and version, word size

Note that not all capabilities are available on all platforms; a more detailed capabilities matrix is available below.  
All structures have an `is_supported` member to allow the user to detect whether or not results could be gathered.

## Requirements and Supported Platforms

Currently supported CPU architectures are x86 and (in a limited manner) ARM.  
General toolchain rule: a reasonably modern OS and a C++ compiler supporting C++11.

- **Windows:** Windows XP or later; tested on Windows 7 and 10 with Visual Studio 2015 and 2017
- **Linux:** Linux 2.6 or later; tested on 4.x with g++ 5.4 and 6.3
- **macOS:** macOS 10.0 or later; tested on 10.10 with AppleClang
- **BSD:** completely untested, likely does not even build yet

## Example

```c
#include <stdio.h>
#include <watisit/watisit.h>

void print_machine_info()
{
    // Obtain machine info
    wiMachineInfo info = wiGetInfo();
    // Print CPU info
    if (info.cpu.is_supported)
    {
        printf("CPU: %s\n", info.cpu.description);
        printf("Cores: %u cores / %u threads\n", info.cpu.num_physical_cores, info.cpu.num_logical_cores);
    }
    // Print memory info
    if (info.memory.phys.is_supported)
        printf("RAM: %llu/%llu MiB used\n", info.memory.phys.used / 1048576, info.memory.phys.total / 1048576);
    if (info.memory.swap.is_supported)
        printf("Swap: %llu/%llu MiB used\n", info.memory.swap.used / 1048576, info.memory.swap.total / 1048576);
    if (info.memory.virt.is_supported)
        printf("VMem: %llu/%llu MiB used\n", info.memory.virt.used / 1048576, info.memory.virt.total / 1048576);
    // Print OS info
    if (info.os.is_supported)
        printf("OS: %s\n", info.os.description);
    if (info.os.kernel.is_supported)
        printf("Kernel: %s %s\n", info.os.kernel.name, info.os.kernel.version_string);
    if (info.os.distro.is_supported)
        printf("Distro: %s %s\n", info.os.distro.name, info.os.distro.version_string);
}
```
```
CPU: AMD Ryzen 7 2700X Eight-Core Processor
Cores: 8 cores / 16 threads
RAM: 16655/32688 MiB used
VMem: 33734/41904 MiB used
OS: Windows 10 Pro
Kernel: Windows NT 10.0.18362.778
```

## Building

The easiest way to build the project is with CMake; by default this will build a static library.  
There are several CMake configuration options to configure watisit's building behaviour:

| Configuration option | Platforms | Default | Description |
| -------------------: | :-------: | :-----: | :---------- |
| `WATISIT_BUILD_SHARED` | All | Off | Builds a shared (dynamic) library instead of a static one. |
| `WATISIT_EXPORT_SYMBOLS` | All | Off | Controls whether or not to export watisit's symbols in the resulting library. For static libraries this is usually unnecessary, but it is required for shared libraries. |
| `WATISIT_USE_REGEXES` | All | Off | Uses regexes for some string parsing operations. May be more robust, but is _definitely_ slower. |
| `WATISIT_BUILD_TINYXML2` | macOS | On | On macOS, watisit builds [tinyxml2](https://github.com/leethomason/tinyxml2) into the library. If tinyxml2 is already statically linked elsewhere, this may lead to symbol collisions; this option can be disabled to avoid this. |

Some of these options may be unavailable depending on the values of other options.

Building the library manually can typically be achieved by building `src/*.cpp` with an include path of `include/`.  
If using watisit as a shared library, the preprocessor symbol `WATISIT_IMPORT_SYMBOLS` must be defined when linking against it.  
On some platforms additional link libraries and include directories may be required; consult `CMakeLists.txt` for details.

## Versioning

Once the library is mature enough, releases will follow [semantic versioning](https://semver.org/); major releases may break API and minor releases may break ABI.

Releases will appear as tags. The library is currently in initial development (version 0.x), and thus the API may change frequently.

## Performance

On a modern machine, all watisit information is generally gathered in well under a millisecond.  
A simple testing application, `watisit_benchmark`, is included and may be run to profile how long the checks take in a given situation.

## License

watisit is released under the MIT license.  
Some work on this project was done whilst I was employed by [Ultrahaptics](https://www.ultrahaptics.com), who graciously agreed that this project could be open-sourced.

## Detailed Capabilities

### CPU info

| Property                             | x86 | ARM |
| :----------------------------------- | :-: | :-: |
| Vendor name                          |  ✓  |  ✗  |
| CPU model description                |  ✓  |  ✗  |
| CPU physical core count              |  ✓  |  ✗  |
| CPU logical core count               |  ✓  |  ✗  |
| Total system logical core count      |  ✓  |  ✓  |
| Word size                            |  ✓  |  ✗  |
| Detailed family/model/stepping info  |  ✓  |  ✗  |
| SIMD detection (e.g. SSE/AVX, NEON)  |  ✓  |  ✓¹ |
| Other feature flag detection         |  ✓  |  ✗  |

¹ Currently only available on Linux.

### Memory info

| Property                          | Windows | Linux | macOS | BSD |
| :-------------------------------- | :-----: | :---: | :---: | :-: |
| Physical memory (used/free/total) |    ✓    |   ✓   |   ✓   |  ✗  |
| Swap file usage (used/free/total) |    ✗    |   ✓   |   ✓¹  |  ✗  |
| Virtual memory (used/free/total)  |    ✓    |   ✓   |   ✗   |  ✗  |

¹ macOS's swap file may use the entire free space on the OS partition if required; this is the value returned.

### OS info

| Property                          | Windows | Linux | macOS | BSD |
| :-------------------------------- | :-----: | :---: | :---: | :-: |
| Description                       |    ✓    |   ✓¹  |   ✓   |  ✗  |
| Word size                         |    ✓    |   ✓²  |   ✓²  |  ✗  |
| Kernel info (name, version)       |    ✓    |   ✓   |   ✓   |  ✗  |
| Distro info (name, version)       |   n/a   |   ✓¹  |  n/a  |  ✗  |

¹ Distro details are currently obtained by parsing `/etc/os-release` and `/etc/lsb-release`, which may not exist on all distros.  
² Word size is obtained by checking the machine result of `uname()` against a known architecture list; this may be error-prone.

## TODO

- Document API via doc comments and incorporate optional doxygen (?) support into build process
- **OS:** Finish implementing and test BSD support (FreeBSD, OpenBSD, NetBSD, others?)
- **OS:** Investigate additional options for Linux distro detection
- **CPU:** Better support for ARM (more CPU info, ideally v6 through to v8)
- **CPU:** Investigate options for handling multi-CPU situations
- **GPU:** Investigate options for GPU detection (may be awkward on most platforms)
