cmake_minimum_required(VERSION 3.7)

project("watisit")

include(CMakeDependentOption)

# ###
# C++ language/toolchain settings
# ###
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)

set(CMAKE_CXX_VISIBILITY_PRESET "hidden")
set(CMAKE_CXX_VISIBILITY_INLINES_HIDDEN TRUE)
set(CMAKE_POSITION_INDEPENDENT_CODE TRUE)

# ###
# Config options
# ###
option(WATISIT_BUILD_SHARED "Build watisit as a shared library" OFF)
# If we're building a static library, allow enabling/disabling the exporting of symbols
CMAKE_DEPENDENT_OPTION(
    WATISIT_EXPORT_SYMBOLS "Export API-visible symbols from the resulting library" OFF
    "NOT WATISIT_BUILD_SHARED" ON)
option(WATISIT_USE_REGEXES "Use regexes for some string parsing operations - potentially more robust, but slower" OFF)
if (APPLE)
    # If we're building a static library, allow enabling/disabling building TinyXML2 into the library
    CMAKE_DEPENDENT_OPTION(
        WATISIT_BUILD_TINYXML2 "Build TinyXML2 into the library on macOS" ON
        "NOT WATISIT_BUILD_SHARED" ON)
endif ()

# ###
# Source files
# ###
set (sources
    "src/cpu.cpp"
    "src/cpu_x86.cpp"
    "src/cpu_arm.cpp"
    "src/cpu_unknown.cpp"
    "src/memory_windows.cpp"
    "src/memory_linux.cpp"
    "src/memory_bsd.cpp"
    "src/memory_macos.cpp"
    "src/memory_unknown.cpp"
    "src/os_windows.cpp"
    "src/os_linux.cpp"
    "src/os_bsd.cpp"
    "src/os_macos.cpp"
    "src/os_unknown.cpp"
    "src/os_util.cpp"
    "src/os_util.hpp"
    "src/util.cpp"
    "src/util.hpp"
)

set (headers
    "include/watisit/common.h"
    "include/watisit/cpu_arm.h"
    "include/watisit/cpu_x86.h"
    "include/watisit/cpu.h"
    "include/watisit/memory.h"
    "include/watisit/os.h"
    "include/watisit/watisit.h"
)

# ###
# Library settings
# ###
if (WATISIT_BUILD_SHARED)
    set (library_type SHARED)
else ()
    set (library_type STATIC)
endif ()

# ###
# TARGET: Library
# ###
add_library(watisit ${library_type} ${sources} ${headers})
target_include_directories(watisit
    PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include"
    PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/src")

if (WATISIT_EXPORT_SYMBOLS)
    # When we're building, export symbols. When other things link to us, import them.
    target_compile_definitions(watisit PRIVATE "-DWATISIT_EXPORT_SYMBOLS" INTERFACE "-DWATISIT_IMPORT_SYMBOLS")
endif ()
if (WATISIT_USE_REGEXES)
    target_compile_definitions(watisit PRIVATE "-DWATISIT_USE_REGEX")
endif ()

# ###
# Dependencies
# ###
if (WIN32)
    target_link_libraries(watisit PRIVATE version kernel32)
elseif (APPLE)
    # tinyxml2 is required for OSX version parsing
    target_include_directories(watisit PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/dependencies/tinyxml2-5.0.1")
    if (WATISIT_BUILD_TINYXML2)
        target_sources(watisit PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/dependencies/tinyxml2-5.0.1/tinyxml2.cpp")
    endif ()
endif ()

# ###
# TARGET: Test applications
# ###
add_executable(watisit_example "test/example.cpp")
target_link_libraries(watisit_example watisit)
add_executable(watisit_benchmark "test/benchmark.cpp")
target_link_libraries(watisit_benchmark watisit)
add_executable(watisit_c_example "test/c_example.c")
target_link_libraries(watisit_c_example watisit)