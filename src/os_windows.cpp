#include "watisit/common.h"

#if defined(WATISIT_OS_WINDOWS)

#include "watisit/os.h"
#include "util.hpp"

#include <string>

#include <windows.h>

static const char* os_name = "Windows";
static const char* kernel_name = "Windows NT";

using FN_RTLGETVERSION = void(WINAPI*)(OSVERSIONINFOEXW*);
static const FN_RTLGETVERSION fnRtlGetVersion = (FN_RTLGETVERSION)::GetProcAddress(::GetModuleHandleA("ntdll"), "RtlGetVersion");

static bool os_windows_get_kernel_version_from_rtl_function(wiOSComponentInfo& cinfo)
{
    if (!fnRtlGetVersion)
        return false;

    OSVERSIONINFOEXW vinfo;
    vinfo.dwOSVersionInfoSize = sizeof(vinfo);
    fnRtlGetVersion(&vinfo);

    cinfo.version_major = vinfo.dwMajorVersion;
    cinfo.version_minor = vinfo.dwMinorVersion;
    cinfo.version_patch = vinfo.dwBuildNumber;
    cinfo.version_build = 0;
    snprintf(cinfo.version_string, WATISIT_OS_VERSION_STRING_LENGTH, "%u.%u.%u.%u",
        cinfo.version_major, cinfo.version_minor, cinfo.version_patch, cinfo.version_build);
    cinfo.is_supported = true;

    return true;
}

static bool os_windows_get_kernel_version_from_dll(wiOSComponentInfo& cinfo)
{
    static const std::string kernel_dll("\\kernel32.dll");

    char path[MAX_PATH] = {0};
    UINT n = ::GetSystemDirectoryA(path, MAX_PATH);
    if (n <= 0 || n > (MAX_PATH - kernel_dll.size())) return false;
    std::string filepath = std::string(path) + kernel_dll;

    DWORD versz = ::GetFileVersionInfoSizeA(filepath.c_str(), nullptr);
    if (versz == 0) return false;
    char* ver = new char[versz];

    void* block;
    UINT blocksz;
    if (::GetFileVersionInfoA(filepath.c_str(), 0, versz, (void*)ver) == 0) return false;
    if (::VerQueryValueA((void*)ver, "\\", &block, &blocksz) == 0 || blocksz < sizeof(VS_FIXEDFILEINFO)) return false;

    VS_FIXEDFILEINFO* vinfo = static_cast<VS_FIXEDFILEINFO*>(block);
    cinfo.version_major = HIWORD(vinfo->dwProductVersionMS);
    cinfo.version_minor = LOWORD(vinfo->dwProductVersionMS);
    cinfo.version_patch = HIWORD(vinfo->dwProductVersionLS);
    cinfo.version_build = LOWORD(vinfo->dwProductVersionLS);
    snprintf(cinfo.version_string, WATISIT_OS_VERSION_STRING_LENGTH, "%u.%u.%u.%u",
        cinfo.version_major, cinfo.version_minor, cinfo.version_patch, cinfo.version_build);
    cinfo.is_supported = true;
    delete[] ver;

    return true;
}

using FN_BRANDINGFORMATSTRING = PWSTR(WINAPI*)(PCWSTR pstrFormat);
static HMODULE libWinbrand = ::LoadLibraryExA("winbrand", nullptr, LOAD_LIBRARY_SEARCH_SYSTEM32);
static FN_BRANDINGFORMATSTRING fnBrandingFormatString = (FN_BRANDINGFORMATSTRING)::GetProcAddress(libWinbrand, "BrandingFormatString");

static bool os_windows_get_description_from_winbrand(wiOSInfo& info)
{
    if (!fnBrandingFormatString)
        return false;
    
    PWSTR str = fnBrandingFormatString(L"%WINDOWS_LONG%");
    if (!str)
        return false;

    int len = static_cast<int>(wcslen(str));
    int size_needed = ::WideCharToMultiByte(CP_UTF8, 0, str, len, nullptr, 0, nullptr, nullptr);
    if (size_needed <= WATISIT_OS_DESCRIPTION_LENGTH)
        ::WideCharToMultiByte(CP_UTF8, 0, str, len, info.description, WATISIT_OS_DESCRIPTION_LENGTH, nullptr, nullptr);
    ::GlobalFree(str);

    return (size_needed <= WATISIT_OS_DESCRIPTION_LENGTH);
}

static bool os_windows_get_description_from_registry(wiOSInfo& info)
{
    HKEY reg_ntcv;
    const char* subkey = "Software\\Microsoft\\Windows NT\\CurrentVersion";
    if (::RegOpenKeyExA(HKEY_LOCAL_MACHINE, subkey, 0, KEY_READ, &reg_ntcv) == ERROR_SUCCESS)
    {
        DWORD len = 128;
        BYTE buf[128] = {0};
        std::string name, csd;
        if (::RegQueryValueExA(reg_ntcv, "ProductName", nullptr, nullptr, buf, &len) == ERROR_SUCCESS)
            name = std::string(reinterpret_cast<char*>(buf));
        if (::RegQueryValueExA(reg_ntcv, "CSDVersion", nullptr, nullptr, buf, &len) == ERROR_SUCCESS)
            csd = std::string(reinterpret_cast<char*>(buf));
        ::RegCloseKey(reg_ntcv);
        
        std::string desc(csd.size() ? name + std::string(" ") + csd : name);
        cstrcopy(info.description, WATISIT_OS_DESCRIPTION_LENGTH, desc.c_str(), desc.size());
        return true;
    }
    return false;
}

// Function definition for IsWow64Process
using FN_WOW64 = BOOL (WINAPI*)(HANDLE, PBOOL);
static const FN_WOW64 fnIsWow64 = (FN_WOW64) ::GetProcAddress(::GetModuleHandleA("kernel32"), "IsWow64Process");

static uint32_t os_get_word_size()
{
#if defined(WATISIT_ARCHBITS) && WATISIT_ARCHBITS == 64
    // We're running in 64-bit mode...
    return 64;
#else
    BOOL isWow64 = FALSE;
    if (fnIsWow64 != nullptr)
    {
        if (fnIsWow64(::GetCurrentProcess(), &isWow64))
            return (isWow64 == TRUE ? 64 : 32);
        else
            return 0;
    }
    else
    {
        // Ancient Windows, so 32-bit
        return 32;
    }
#endif
}


#if defined(__cplusplus)
extern "C" {
#endif

bool wiOSPopulateInfo(wiOSInfo* info)
{
    if (!info)
        return false;
    memset(info, '\0', sizeof(wiOSInfo));

    info->os = wiOS_Windows;
    cstrcopy(info->name, WATISIT_OS_SHORT_NAME_LENGTH, os_name, strlen(os_name));
    
    // Kernel name + version
    cstrcopy(info->kernel.name, WATISIT_OS_COMPONENT_NAME_LENGTH, kernel_name, strlen(kernel_name));
    if (!os_windows_get_kernel_version_from_rtl_function(info->kernel))
        os_windows_get_kernel_version_from_dll(info->kernel);

    // Description
    if (!os_windows_get_description_from_winbrand(*info))
        os_windows_get_description_from_registry(*info);

    info->word_size = os_get_word_size();

    info->is_supported = true;
    return info->is_supported;
}

#if defined(__cplusplus)
}
#endif

#endif