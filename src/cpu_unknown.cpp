#include "watisit/common.h"

#if defined(WATISIT_ARCH_UNKNOWN)

#include "watisit/cpu.h"
#include "util.hpp"

#if defined(__cplusplus)
extern "C" {
#endif

bool wiCPUPopulateInfo(wiCPUInfo* info)
{
    if (!info)
        return false;
    memset(info, '\0', sizeof(wiCPUInfo));
    return false;
}

#if defined(__cplusplus)
}
#endif

#endif
