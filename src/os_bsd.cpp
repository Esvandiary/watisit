#include "watisit/common.h"

#if defined(WATISIT_OS_BSD)

#include "watisit/os.h"
#include "os_util.hpp"

#include <sys/utsname.h>

static const char* os_name_freebsd = "FreeBSD";
static const char* os_name_openbsd = "OpenBSD";
static const char* os_name_netbsd  = "NetBSD";

static bool os_bsd_get_kernel_info(wiOSInfo& info)
{
    struct utsname un;
    if (uname(&un) == 0)
    {
        os_unix_get_kernel_info(info, un);

        info.kernel.is_supported = true;
    }
    return info.kernel.is_supported;
}

#if defined(__cplusplus)
extern "C" {
#endif

bool wiOSPopulateInfo(wiOSInfo* info)
{
    if (!info)
        return false;
    memset(info, '\0', sizeof(wiOSInfo));

    os_bsd_get_kernel_info(*info);
    // os_bsd_get_distro_info(info);

    // TODO: Check kernel name against known ones
    // cstrcopy(info->name, OS_SHORT_NAME_LENGTH, info->kernel.name, sizeof(info->kernel.name));

    info->is_supported = true;
    return info->is_supported;
}

#if defined(__cplusplus)
}
#endif

#endif // OS