#include "watisit/common.h"

#if defined(WATISIT_OS_MACOS)

#include "watisit/memory.h"
#include "util.hpp"

#include <string>
#include <cstring>
#include <fstream>
#include <map>

#include <sys/types.h>
#include <sys/sysctl.h>
#include <sys/param.h>
#include <sys/mount.h>
#include <mach/vm_statistics.h>
#include <mach/mach_types.h>
#include <mach/mach_init.h>
#include <mach/mach_host.h>
#include <mach/mach.h>

#if defined(__cplusplus)
extern "C" {
#endif

bool wiMemoryPopulateInfo(wiMemoryInfo* info)
{
    if (!info)
        return false;
    memset(info, '\0', sizeof(wiMemoryInfo));

    // Swap
    // OS X uses all available space on the boot partition as swap (?!) so find out what that is
    struct statfs sts;
    if (statfs("/", &sts) == 0)
        info->swap.limit = static_cast<uint64_t>(sts.f_bsize) * static_cast<uint64_t>(sts.f_bfree);
    // Used swap
    int sw_mib[2] { CTL_VM, VM_SWAPUSAGE };
    xsw_usage swu = {0};
    size_t swu_length = sizeof(swu);
    if (info->swap.limit != 0 && sysctl(sw_mib, 2, &swu, &swu_length, nullptr, 0) == 0)
    {
        info->swap.total = swu.xsu_total;
        info->swap.available = swu.xsu_avail;
        info->swap.used = swu.xsu_used;
        info->swap.is_supported = true;
    }
    // Total physical RAM
    int pr_mib[2] { CTL_HW, HW_MEMSIZE };
    size_t i64_length = sizeof(int64_t);
    int64_t phys_total = 0;
    if (sysctl(pr_mib, 2, &phys_total, &i64_length, nullptr, 0) == 0)
        info->phys.limit = info->phys.total = static_cast<uint64_t>(phys_total);
    // Available RAM
    mach_port_t port = mach_host_self();
    vm_statistics64_data_t vms;
    vm_size_t psize;
    mach_msg_type_number_t count = sizeof(vms) / sizeof(natural_t);
    if (phys_total != 0 && host_page_size(port, &psize) == KERN_SUCCESS && host_statistics64(port, HOST_VM_INFO, (host_info64_t)&vms, &count) == KERN_SUCCESS)
    {
        info->phys.used = (static_cast<uint64_t>(vms.active_count) + static_cast<uint64_t>(vms.inactive_count)
                          + static_cast<uint64_t>(vms.wire_count)) * static_cast<uint64_t>(psize);
        info->phys.available = info.phys.total - info.phys.used;
        info->phys.is_supported = true;
    }
    info->is_supported = (info->phys.is_supported || info->swap.is_supported);
    return true;
}

#if defined(__cplusplus)
}
#endif

#endif