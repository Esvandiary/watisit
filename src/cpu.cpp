#include "watisit/common.h"

#include "watisit/cpu.h"

// Logical cores check
#include <thread>
#if defined(WATISIT_OS_WINDOWS)
    #include <windows.h>
#elif defined(WATISIT_OS_LINUX)
    #include <sys/sysinfo.h>
    #include <unistd.h>
#elif defined(WATISIT_OS_MACOS)
    #include <unistd.h>
    #include <mach/clock_types.h>
    #include <mach/clock.h>
    #include <mach/mach.h>
#elif defined(WATISIT_OS_BSD)
    #include <sys/types.h>
    #include <sys/sysctl.h>
#endif

#if defined(__cplusplus)
extern "C" {
#endif

// Code adapted from https://github.com/anrieff/libcpuid
uint32_t wiGetTotalLogicalCPUCount(void)
{
#if defined(WATISIT_OS_MACOS)
    kern_return_t kr;
    host_basic_info_data_t basic_info;
    host_info_t info = (host_info_t)&basic_info;
    host_flavor_t flavor = HOST_BASIC_INFO;
    mach_msg_type_number_t count = HOST_BASIC_INFO_COUNT;
    kr = host_info(mach_host_self(), flavor, info, &count);
    if (kr != KERN_SUCCESS) return 0UL;
    return static_cast<uint32_t>(basic_info.avail_cpus);
#elif defined(WATISIT_OS_WINDOWS)
    SYSTEM_INFO system_info;
    ::GetSystemInfo(&system_info);
    return static_cast<uint32_t>(system_info.dwNumberOfProcessors);
#elif defined(WATISIT_OS_LINUX)
    return static_cast<uint32_t>(sysconf(_SC_NPROCESSORS_ONLN));
#elif defined(WATISIT_OS_BSD)
    int mib[2] = { CTL_HW, HW_NCPU };
    int ncpus;
    size_t len = sizeof(ncpus);
    if (sysctl(mib, 2, &ncpus, &len, (void *) 0, 0) != 0) return 0UL;
    return static_cast<uint32_t>(ncpus);
#else
    return std::thread::hardware_concurrency();
#endif
}

#if defined(__cplusplus)
}
#endif
