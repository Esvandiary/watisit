#include "watisit/common.h"

#if defined(WATISIT_OS_UNKNOWN)

#include "watisit/memory.h"
#include "util.hpp"

#if defined(__cplusplus)
extern "C" {
#endif

bool wiMemoryPopulateInfo(wiMemoryInfo* info)
{
    if (!info)
        return false;
    memset(info, '\0', sizeof(wiMemoryInfo));
    return false;
}

#if defined(__cplusplus)
}
#endif

#endif