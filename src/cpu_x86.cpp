#include "watisit/common.h"

#if defined(WATISIT_ARCH_X86)

#include "watisit/cpu.h"
#include "util.hpp"

#include <cstring>
#include <cstdint>
#include <cassert>

// CPUID instruction
#if defined(WATISIT_COMPILER_MSVC)
    #include <intrin.h>
#elif defined(WATISIT_COMPILER_GCC) || defined(WATISIT_COMPILER_CLANG)
    #include <cpuid.h>
#endif


static const char* x86_vendor_strings[] = {
    "GenuineIntel",
    "AuthenticAMD",
    "CyrixInstead",
    "NexGenDriven",
    "GenuineTMx86",
    "UMC UMC UMC ",
    "CentaurHauls",
    "RiseRiseRise",
    "SiS SiS SiS ",
    "Geode by NSC",
    "VIA VIA VIA ",
    nullptr
};

static const char* x86_vendor_names[] = {
    "Intel",
    "AMD",
    "Cyrix",
    "Nexgen",
    "Transmeta",
    "UMC",
    "Centaur",
    "Rise",
    "SiS",
    "NSC",
    "VIA",
    nullptr
};

static const size_t max_intel_fn11_count = 4UL;


static void exec_cpuidex_direct(uint32_t eax, uint32_t sub, uint32_t* regs)
{
#if defined(WATISIT_COMPILER_MSVC)
    __cpuidex(reinterpret_cast<int*>(regs), static_cast<int>(eax), static_cast<int>(sub));
#elif defined(WATISIT_COMPILER_GCC) || defined(WATISIT_COMPILER_CLANG)
    // GCC's __cpuid* will potentially do the wrong thing on x86-32 with PIC enabled
    // Clang's __cpuid* seems to just be broken on x86-64 for anything that wants to access results in ebx
    asm volatile (
    #if defined(WATISIT_OS_X86_32) && defined(__PIC__)
        "mov %%ebx, %%edi;"
        "cpuid;"
        "xchgl %%ebx, %%edi;"
            :   "=a" (regs[0]),
                "=D" (regs[1]),
    #else
        "cpuid"
            :   "=a" (regs[0]),
                "=b" (regs[1]),
    #endif
                "=c" (regs[2]),
                "=d" (regs[3])
            :   "a" (eax),
                "c" (sub));
#else
    #error "No exec_cpuidex_direct function available for this platform on x86 architecture"
#endif
}

static void exec_cpuid_direct(uint32_t eax, uint32_t* regs)
{
    exec_cpuidex_direct(eax, 0x0, regs);
}


static void exec_cpuid(uint32_t eax, uint32_t* regs)
{
    regs[0] = eax;
    regs[1] = 0;
    regs[2] = 0;
    regs[3] = 0;
    exec_cpuid_direct(eax, regs);
}


static uint32_t cpuid_get_max(uint32_t eax)
{
#if defined(WATISIT_COMPILER_GCC) || defined(WATISIT_COMPILER_CLANG)
    return __get_cpuid_max(eax, nullptr);
#elif defined(WATISIT_COMPILER_MSVC)
    int data[4] { 0, 0, 0, 0 };
    __cpuid(data, static_cast<int>(eax));
    return static_cast<uint32_t>(data[0]);
#else
    return 0;
#endif
}


static bool cpuid_available()
{
#if defined(WATISIT_ARCH_X86_64)
    return true; // CPUID is always present on x86_64
#else
#  if defined(WATISIT_COMPILER_GCC) || defined(WATISIT_COMPILER_CLANG)
    // get_cpuid_max checks whether CPUID is present before attempting to access it
    return (__get_cpuid_max(0x00000000, nullptr) && __get_cpuid_max(0x80000000, nullptr));
#  elif defined(WATISIT_COMPILER_MSVC)
    // ASM originally from https://github.com/anrieff/libcpuid
    int result;
    __asm {
        pushfd
        pop    eax
        mov    ecx,    eax
        xor    eax,    0x200000
        push    eax
        popfd
        pushfd
        pop    eax
        xor    eax,    ecx
        mov    result,    eax
        push    ecx
        popfd
    };
    return (result != 0);
#  else
    return false;
#  endif
#endif
}


static bool check_xcr0_ymm() 
{
    uint32_t xcr0;
#if defined(WATISIT_COMPILER_MSVC)
    xcr0 = (uint32_t)_xgetbv(0);
#elif defined(WATISIT_COMPILER_GCC) || defined(WATISIT_COMPILER_CLANG)
    __asm__ ("xgetbv" : "=a" (xcr0) : "c" (0) : "%edx" );
#else
    #error "No compiler support for XGETBV check"
#endif
    return ((xcr0 & 6) == 6); // checking if xmm and ymm state are enabled in XCR0
}


static int get_sse_size(const wiCPUDetailsX86& data, wiCPUVendorX86 vendor, uint32_t max0, uint32_t max8)
{
    if (vendor == wiCPUVendorX86_AMD)
    {
        if (max8 >= 0x8000001AU)
        {
            uint32_t regs[4];
            exec_cpuid(0x8000001AU, regs);
            return (regs[0] & 1) ? 128 : 64;
        }
        else
        {
            return (data.ext_family >= 16 && data.ext_family != 17) ? 128 : 64;
        }
    }
    else if (vendor == wiCPUVendorX86_Intel)
    {
        return (data.family == 6 && data.ext_model >= 15) ? 128 : 64;
    }
    else
    {
        // ??? We probably shouldn't get here since we'd need an SSE flag to do so
        return 0;
    }
}


// Code adapted from https://github.com/anrieff/libcpuid
static bool cpu_x86_get_core_details(wiCPUInfo& info, uint32_t max0, uint32_t max8)
{
    if (info.details.vendor == wiCPUVendorX86_Intel && max0 >= 0xB)
    {
        int num_smt = 0UL, num_core = 0UL;
        uint32_t regs[4];
        // EAX = 11, ECX = ?
        for (uint32_t i = 0; i < 8; ++i)
        {
            exec_cpuidex_direct(11, i, regs);
            int level_type = (regs[2] & 0xff00) >> 8;
            if (level_type == 0x01)
                num_smt = regs[1] & 0xffff;
            else if (level_type == 0x02)
                num_core = regs[1] & 0xffff;
        }
        if (num_smt == -1 || num_core == -1) return false;
        info.num_logical_cores = num_core;
        info.num_physical_cores = num_core / num_smt;
        return true;
    }
    else if (info.details.vendor == wiCPUVendorX86_AMD)
    {
        if (max0 >= 0x1U)
        {
            uint32_t regs[4];
            exec_cpuid(0x00000001U, regs);
            info.num_logical_cores = (regs[1] >> 16) & 0xFF;
            if (max8 >= 0x80000008U)
            {
                exec_cpuid(0x80000008U, regs);
                info.num_physical_cores = 1U + (regs[2] & 0xFF);
            }
        }
        if (info.features.flags.HT)
        {
            if (info.num_physical_cores > 1)
            {
                // TODO: Check this is a reasonable thing to do?
                if (info.details.ext_family >= 23)
                    info.num_physical_cores /= 2; // e.g., Ryzen 7 reports 16 "real" cores, but they are really just 8.
            }
            else
            {
                info.num_logical_cores = (info.num_logical_cores >= 2 ? info.num_logical_cores : 2);
            }
        }
        else
        {
            info.num_physical_cores = info.num_logical_cores = 1;
        }
        return true;
    }
    return false;
}


static bool cpu_x86_get_details(wiCPUDetailsX86& data, wiCPUVendorX86 vendor, uint32_t max0, uint32_t max8)
{
    uint32_t regs[4];
    uint32_t eax1 = 0U, ebx1 = 0U;
    if (max0 >= 0x00000001U)
    {
        exec_cpuid(0x000000001U, regs);
        eax1 = regs[0]; ebx1 = regs[1];
    }
    data.family   = (eax1 >> 8) & 0xF;
    data.model    = (eax1 >> 4) & 0xF;
    data.stepping = (eax1 >> 0) & 0xF;
    // Quoth Wikipedia: Intel and AMD have suggested applications to display the family of a CPU as the sum of the "Family"
    // and the "Extended Family" fields, and the model as the sum of the "Model" and the 4-bit left-shifted "Extended Model" fields.
    // If "Family" is set to 15, then "Extended Family" and the 4-bit left-shifted "Extended Model" should be added to the respective
    // base values, and if "Family" is set to 6, then only the 4-bit left-shifted "Extended Model" should be added to "Model".
    if (data.family == 15U || data.family == 6U)
    {
        data.ext_family = data.family + (data.family == 15U ? ((eax1 >> 20) & 0xF) : 0U);
        data.ext_model  = data.model + (((eax1 >> 16) & 0xF) << 4);
    }

    data.sse_size = get_sse_size(data, vendor, max0, max8);
    return true;
}


static bool cpu_x86_get_features(wiCPUFeaturesX86& f, wiCPUVendorX86 vendor, uint32_t max0, uint32_t max8)
{
    uint32_t regs[4];
    uint32_t ecx1 = 0U, edx1 = 0U, ebx7 = 0U, ecx81 = 0U, edx81 = 0U, edx87 = 0U;
    if (max0 >= 0x00000001U)
    {
        exec_cpuid(0x00000001U, regs);
        ecx1 = regs[2]; edx1 = regs[3];
    }
    if (max0 >= 0x00000007U)
    {
        exec_cpuid(0x00000007U, regs);
        ebx7 = regs[1];
    }
    if (max8 >= 0x80000001U)
    {
        exec_cpuid(0x80000001U, regs);
        ecx81 = regs[2]; edx81 = regs[3];
    }
    if (max8 >= 0x80000007U)
    {
        exec_cpuid(0x80000007U, regs);
        edx87 = regs[3];
    }

    // 0x1, EDX
    f.flags.FPU  = is_reg_bit_set(edx1,  0);
    f.flags.VME  = is_reg_bit_set(edx1,  1);
    f.flags.DE   = is_reg_bit_set(edx1,  2);
    f.flags.PSE  = is_reg_bit_set(edx1,  3);
    f.flags.TSC  = is_reg_bit_set(edx1,  4);
    f.flags.MSR  = is_reg_bit_set(edx1,  5);
    f.flags.PAE  = is_reg_bit_set(edx1,  6);
    f.flags.MCE  = is_reg_bit_set(edx1,  7);
    f.flags.CX8  = is_reg_bit_set(edx1,  8);
    f.flags.APIC = is_reg_bit_set(edx1,  9);
    f.flags.SEP  = is_reg_bit_set(edx1, 11);
    f.flags.MTRR = is_reg_bit_set(edx1, 12);
    f.flags.PGE  = is_reg_bit_set(edx1, 13);
    f.flags.MCA  = is_reg_bit_set(edx1, 14);
    f.flags.CMOV = is_reg_bit_set(edx1, 15);
    f.flags.PAT  = is_reg_bit_set(edx1, 16);
    f.flags.PSE36   = is_reg_bit_set(edx1, 17);
    f.flags.CLFLUSH = is_reg_bit_set(edx1, 19);
    f.flags.MMX  = is_reg_bit_set(edx1, 23);
    f.flags.FXSR = is_reg_bit_set(edx1, 24);
    f.flags.SSE  = is_reg_bit_set(edx1, 25);
    f.flags.SSE2 = is_reg_bit_set(edx1, 26);
    f.flags.HT   = is_reg_bit_set(edx1, 28);
    // 0x1, ECX
    f.flags.SSE3   = is_reg_bit_set(ecx1,  0);
    f.flags.PCLMUL = is_reg_bit_set(ecx1,  1);
    f.flags.MONITOR= is_reg_bit_set(ecx1,  3);
    f.flags.SSSE3  = is_reg_bit_set(ecx1,  9);
    f.flags.FMA3   = is_reg_bit_set(ecx1, 12);
    f.flags.CX16   = is_reg_bit_set(ecx1, 13);
    f.flags.SSE4_1 = is_reg_bit_set(ecx1, 19);
    f.flags.SSE4_2 = is_reg_bit_set(ecx1, 20);
    f.flags.MOVBE  = is_reg_bit_set(ecx1, 22);
    f.flags.POPCNT = is_reg_bit_set(ecx1, 23);
    f.flags.AES    = is_reg_bit_set(ecx1, 25);
    f.flags.XSAVE  = is_reg_bit_set(ecx1, 26);
    f.flags.OSXSAVE= is_reg_bit_set(ecx1, 27);
    f.flags.AVX    = is_reg_bit_set(ecx1, 28);
    f.flags.F16C   = is_reg_bit_set(ecx1, 29);
    f.flags.RDRAND = is_reg_bit_set(ecx1, 30);
    // 0x7, EBX
    f.flags.BMI1 = is_reg_bit_set(ebx7, 3);
    f.flags.AVX2 = is_reg_bit_set(ebx7, 5);
    f.flags.BMI2 = is_reg_bit_set(ebx7, 8);
    // 0x80000001, EDX
    f.flags.SYSCALL = is_reg_bit_set(edx81, 11);
    f.flags.RDTSCP  = is_reg_bit_set(edx81, 27);
    f.flags.LM      = is_reg_bit_set(edx81, 29);
    // 0x80000001, ECX
    f.flags.LAHF_LM = is_reg_bit_set(ecx81,  0);
    // 0x80000007, EDX
    f.flags.CONSTANT_TSC = is_reg_bit_set(edx87, 8);

    if (vendor == wiCPUVendorX86_Intel)
    {
        // 0x1, EDX
        f.flags.PN   = is_reg_bit_set(edx1, 18);
        f.flags.DTS  = is_reg_bit_set(edx1, 21);
        f.flags.ACPI = is_reg_bit_set(edx1, 22);
        f.flags.SS   = is_reg_bit_set(edx1, 27);
        f.flags.TM   = is_reg_bit_set(edx1, 29);
        f.flags.IA64 = is_reg_bit_set(edx1, 30);
        f.flags.PBE  = is_reg_bit_set(edx1, 31);
        // 0x1, ECX
        f.flags.DTS64  = is_reg_bit_set(ecx1,  2);
        f.flags.DS_CPL = is_reg_bit_set(ecx1,  4);
        f.flags.VMX    = is_reg_bit_set(ecx1,  5);
        f.flags.SMX    = is_reg_bit_set(ecx1,  6);
        f.flags.EST    = is_reg_bit_set(ecx1,  7);
        f.flags.TM2    = is_reg_bit_set(ecx1,  8);
        f.flags.CID    = is_reg_bit_set(ecx1, 10);
        f.flags.XTPR   = is_reg_bit_set(ecx1, 14);
        f.flags.PDCM   = is_reg_bit_set(ecx1, 15);
        f.flags.DCA    = is_reg_bit_set(ecx1, 18);
        f.flags.X2APIC = is_reg_bit_set(ecx1, 21);
        // 0x80000001, EDX
        f.flags.XD = is_reg_bit_set(edx81, 20);
        // 0x7, EBX
        f.flags.SGX      = is_reg_bit_set(ebx7, 2);
        f.flags.HLE      = is_reg_bit_set(ebx7,  4);
        f.flags.RTM      = is_reg_bit_set(ebx7, 11);
        f.flags.AVX512F  = is_reg_bit_set(ebx7, 16);
        f.flags.AVX512DQ = is_reg_bit_set(ebx7, 17);
        f.flags.RDSEED   = is_reg_bit_set(ebx7, 18);
        f.flags.ADX      = is_reg_bit_set(ebx7, 19);
        f.flags.AVX512PF = is_reg_bit_set(ebx7, 26);
        f.flags.AVX512ER = is_reg_bit_set(ebx7, 27);
        f.flags.AVX512CD = is_reg_bit_set(ebx7, 28);
        f.flags.SHA_NI   = is_reg_bit_set(ebx7, 29);
        f.flags.AVX512BW = is_reg_bit_set(ebx7, 30);
        f.flags.AVX512VL = is_reg_bit_set(ebx7, 31);

        // SGX specifics
        if (max0 >= 0x00000012U)
        {
            exec_cpuid(0x00000012U, regs);
            uint32_t eax12 = regs[0];
            f.flags.SGX1 = is_reg_bit_set(eax12, 0);
            f.flags.SGX2 = is_reg_bit_set(eax12, 1);
        }
    }
    else if (vendor == wiCPUVendorX86_AMD)
    {
        // 0x80000001, EDX
        f.flags.NX           = is_reg_bit_set(edx81, 20);
        f.flags.MMXEXT       = is_reg_bit_set(edx81, 22);
        f.flags.FXSR_OPT     = is_reg_bit_set(edx81, 25);
        f.flags.THREEDNOWEXT = is_reg_bit_set(edx81, 30);
        f.flags.THREEDNOW    = is_reg_bit_set(edx81, 31);
        // 0x80000001, ECX
        f.flags.CMP_LEGACY   = is_reg_bit_set(ecx81,  1);
        f.flags.SVM          = is_reg_bit_set(ecx81,  2);
        f.flags.ABM          = is_reg_bit_set(ecx81,  5);
        f.flags.SSE4A        = is_reg_bit_set(ecx81,  6);
        f.flags.MISALIGNSSE  = is_reg_bit_set(ecx81,  7);
        f.flags.PREFETCH     = is_reg_bit_set(ecx81,  8);
        f.flags.OSVW         = is_reg_bit_set(ecx81,  9);
        f.flags.IBS          = is_reg_bit_set(ecx81, 10);
        f.flags.XOP          = is_reg_bit_set(ecx81, 11);
        f.flags.SKINIT       = is_reg_bit_set(ecx81, 12);
        f.flags.WDT          = is_reg_bit_set(ecx81, 13);
        f.flags.FMA4         = is_reg_bit_set(ecx81, 16);
        f.flags.TBM          = is_reg_bit_set(ecx81, 21);
        // 0x80000007, EDX
        f.flags.TS           = is_reg_bit_set(edx87,  0);
        f.flags.FID          = is_reg_bit_set(edx87,  1);
        f.flags.VID          = is_reg_bit_set(edx87,  2);
        f.flags.TTP          = is_reg_bit_set(edx87,  3);
        f.flags.TM_AMD       = is_reg_bit_set(edx87,  4);
        f.flags.STC          = is_reg_bit_set(edx87,  5);
        f.flags.STEP100MHZ   = is_reg_bit_set(edx87,  6);
        f.flags.HWPSTATE     = is_reg_bit_set(edx87,  7);
        f.flags.CPB          = is_reg_bit_set(edx87,  9);
        f.flags.APERFMPERF   = is_reg_bit_set(edx87, 10);
        f.flags.PFI          = is_reg_bit_set(edx87, 11);
        f.flags.PA           = is_reg_bit_set(edx87, 12);
    }

    // Check for XSAVE and OSXSAVE to ensure XGETBV is present before trying to call it
    bool xcr0_ymm = false;
    if (f.flags.XSAVE && f.flags.OSXSAVE)
    {
        xcr0_ymm = check_xcr0_ymm();
    }

    f.SSE = f.flags.SSE;
    f.SSE2 = f.flags.SSE2;
    f.SSE3 = f.flags.SSE3;
    f.SSSE3 = f.flags.SSSE3;
    f.SSE4_1 = f.flags.SSE4_1;
    f.SSE4_2 = f.flags.SSE4_2;
    f.AVX = (xcr0_ymm && f.flags.AVX);
    f.AVX2 = (xcr0_ymm && f.flags.AVX2);

    return true;
}


#if defined(__cplusplus)
extern "C" {
#endif

bool wiCPUPopulateInfo(wiCPUInfo* info)
{
    if (!info)
        return false;
    memset(info, '\0', sizeof(wiCPUInfo));

    if (!cpuid_available()) return false;

    uint32_t max0 = cpuid_get_max(0x00000000);
    uint32_t max8 = cpuid_get_max(0x80000000);

    uint32_t regs[4];
    exec_cpuid(0x00000000U, regs);
    // CPU vendor string is stored in EBX, EDX, ECX (!!!)
    char vendor_string[WATISIT_CPU_VENDOR_NAME_SIZE];
    memset(vendor_string, 0, WATISIT_CPU_VENDOR_NAME_SIZE);
    memcpy(&(vendor_string[0]), &(regs[1]), sizeof(uint32_t));
    memcpy(&(vendor_string[4]), &(regs[3]), sizeof(uint32_t));
    memcpy(&(vendor_string[8]), &(regs[2]), sizeof(uint32_t));
    info->vendor_name[0] = '\0';

    for (uint32_t i = 0; x86_vendor_strings[i] != nullptr; ++i)
    {
        if (strcmp(vendor_string, x86_vendor_strings[i]) == 0)
        {
            // An arch's value in the CPUArch enum is the base value for that arch's part of the CPUVendor enum
            info->details.vendor = static_cast<wiCPUVendorX86>(wiCPUArch_x86 + i);
            cstrcopy(info->vendor_name, sizeof(info->vendor_name), x86_vendor_names[i], WATISIT_CPU_VENDOR_NAME_SIZE);
            break;
        }
    }

    // If we didn't find a friendly name, just copy in the string from CPUID
    if (info->vendor_name[0] == '\0')
    {
        cstrcopy(info->vendor_name, sizeof(info->vendor_name), vendor_string, WATISIT_CPU_VENDOR_NAME_SIZE);
    }

    // Friendly name ("brand string")
    if (max8 >= 4U)
    {
        static_assert(WATISIT_CPU_DESCRIPTION_SIZE > 48+1, "CPU description field is not large enough for x86 CPUID description");

        exec_cpuid(0x80000002U, regs);
        memcpy(&(info->description[0]), regs, sizeof(regs));
        exec_cpuid(0x80000003U, regs);
        memcpy(&(info->description[16]), regs, sizeof(regs));
        exec_cpuid(0x80000004U, regs);
        memcpy(&(info->description[32]), regs, sizeof(regs));
        info->description[48] = '\0';

        size_t idx = 0;
        while (info->description[idx] == ' ') ++idx;
        if (idx != 0)
            std::memmove(&(info->description[0]), &(info->description[idx]), strlen(info->description)+1);
    }

    if (!cpu_x86_get_details(info->details, info->details.vendor, max0, max8)) return false;
    if (!cpu_x86_get_features(info->features, info->details.vendor, max0, max8)) return false;
    if (!cpu_x86_get_core_details(*info, max0, max8)) return false;

    // LM, or "long mode", indicates that the CPU can go to 64-bit mode
    info->word_size = (info->features.flags.LM ? 64 : 32);

    info->is_supported = true;
    return info->is_supported;
}

}
#endif // ARCH
