#include "watisit/common.h"

#if defined(WATISIT_OS_LINUX)

#include "watisit/os.h"
#include "os_util.hpp"
#include "util.hpp"

#include <string>
#include <cstring>
#include <cstdio>
#include <map>

#if defined(WATISIT_USE_REGEX)
    #include <regex>
#endif

#include <sys/utsname.h>


static const char* os_name = "Linux";

#if defined(WATISIT_USE_REGEX)
    static const std::regex distro_version_regex(R"___(^(\d+)(?:\.(\d+)(?:\.(\d+)(?:\.(\d+))?)?)?.*$)___");
#endif

static bool os_linux_get_kernel_info(wiOSInfo& info)
{
    struct utsname un;
    if (uname(&un) == 0)
    {
        os_unix_get_kernel_info(info, un);
        os_unix_get_kernel_version(info.kernel, un);

        info.kernel.is_supported = true;
    }
    return info.kernel.is_supported;
}

static bool os_linux_get_distro_info(wiOSInfo& info)
{
    auto osr = parse_vardef_file("/etc/os-release");
    auto lsb = parse_vardef_file("/etc/lsb-release");

    std::map<std::string,std::string>::const_iterator it;
    // Distro name
    if ((it = osr.find("NAME")) != osr.end() || (it = lsb.find("DISTRIB_ID")) != lsb.end())
        cstrcopy(info.distro.name, WATISIT_OS_COMPONENT_NAME_LENGTH, it->second.c_str(), it->second.size());

    // Distro version of some sort
    if ((it = osr.find("VERSION")) != osr.end() || (it = osr.find("VERSION_ID")) != osr.end()
        || (it = lsb.find("DISTRIB_RELEASE")) != lsb.end())
    {
        cstrcopy(info.distro.version_string, WATISIT_OS_VERSION_STRING_LENGTH, it->second.c_str(), it->second.size());
        os_get_version_from_string(info.distro, info.distro.version_string, false);
    }

    // Attempt to produce a reasonable description from whatever data is available
    if ((it = osr.find("PRETTY_NAME")) != osr.end())
    {
        cstrcopy(info.description, WATISIT_OS_DESCRIPTION_LENGTH, it->second.c_str(), it->second.size());
    }
    else if (osr.count("NAME") && osr.count("VERSION"))
    {
        const std::string desc = osr["NAME"] + std::string(" ") + osr["VERSION"];
        cstrcopy(info.description, WATISIT_OS_DESCRIPTION_LENGTH, desc.c_str(), desc.size());
    }
    else if ((it = lsb.find("DISTRIB_DESCRIPTION")) != lsb.end())
    {
        cstrcopy(info.description, WATISIT_OS_DESCRIPTION_LENGTH, it->second.c_str(), it->second.size());
    }

    info.distro.is_supported = true;
    return true;
}

#if defined(__cplusplus)
extern "C" {
#endif

bool wiOSPopulateInfo(wiOSInfo* info)
{
    if (!info)
        return false;
    memset(info, '\0', sizeof(wiOSInfo));

    info->os = wiOS_Linux;
    cstrcopy(info->name, WATISIT_OS_SHORT_NAME_LENGTH, os_name, std::strlen(os_name));

    os_linux_get_kernel_info(*info);
    os_linux_get_distro_info(*info);

    info->is_supported = true;
    return info->is_supported;
}

#if defined(__cplusplus)
}
#endif

#endif // OS
