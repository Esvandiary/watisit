#include "watisit/common.h"

#if defined(WATISIT_OS_WINDOWS)

#include "watisit/memory.h"
#include "util.hpp"

#include <windows.h>


#if defined(__cplusplus)
extern "C" {
#endif

bool wiMemoryPopulateInfo(wiMemoryInfo* info)
{
    if (!info)
        return false;
    memset(info, '\0', sizeof(wiMemoryInfo));

    MEMORYSTATUSEX mst { sizeof(mst) };
    if (::GlobalMemoryStatusEx(&mst) == 0) return false;
    info->phys.limit = info->phys.total = mst.ullTotalPhys;
    info->phys.available = mst.ullAvailPhys;
    info->phys.used = info->phys.total - info->phys.available;
    info->phys.is_supported = true;
    // Can't seem to easily get pagefile info, maybe via WMI? (ugh)
    info->virt.limit = info->virt.total = mst.ullTotalPageFile;
    info->virt.available = mst.ullAvailPageFile;
    info->virt.used = info->virt.total - info->virt.available;
    info->virt.is_supported = true;

    info->is_supported = (info->phys.is_supported || info->virt.is_supported);
    return true;
}

#if defined(__cplusplus)
}
#endif

#endif