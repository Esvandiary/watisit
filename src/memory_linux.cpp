#include "watisit/common.h"

#if defined(WATISIT_OS_LINUX)

#include "watisit/memory.h"
#include "util.hpp"

#include <string>
#include <cstring>
#include <fstream>
#include <map>

#if defined(WATISIT_USE_REGEX)
    #include <regex>
#endif

#include <sys/sysinfo.h>
#include <unistd.h>
#include <sys/utsname.h>

#if defined(WATISIT_USE_REGEX)
// The ___( )___ is C++ raw string literal syntax, and isn't actually part of the regex
static const std::regex meminfo_line_regex(R"___(^\s*(\w+):\s*(-?[\d.]+)\s*([kKM]?)B\s*$)___");
#endif

static uint32_t get_multiplier(char c)
{
    if (c == 'k' || c == 'K')
        return (1U<<10);
    if (c == 'M')
        return (1U<<20);
    return 1;
}

// Reads meminfo and parses out each line into a map of {name: value_in_bytes}
static std::map<std::string, uint64_t> get_meminfo_entries(const char* filename = "/proc/meminfo")
{
    std::map<std::string, uint64_t> output;
    std::string line;
    std::ifstream fs(filename);
    while (std::getline(fs, line))
    {
    #if defined(WATISIT_USE_REGEX)
        std::smatch match;
        if (!std::regex_match(line, match, meminfo_line_regex)) continue;
        output[match[1].str()] = std::stoull(match[2].str()) * get_multiplier(*match[3].str().c_str());
    #else
        trim(line);
        const size_t colpos = line.find(':');
        if (colpos == std::string::npos) continue;
        std::string name = line.substr(0, colpos);
        std::string value = line.substr(colpos + 1);
        if (value.back() != 'B') continue;
        const char mulchar = value[value.size()-2];
        const uint32_t mult = get_multiplier(mulchar);
        const bool has_suffix = (mult != 1);
        value = value.substr(0, value.size() - (has_suffix ? 2 : 1));
        trim(value);
        output[name] = std::stoull(value) * mult;
    #endif
    }
    return output;
}

// WSL has "Microsoft" in the kernel version string, may also have "WSL"
static bool check_if_wsl(const char* filename = "/proc/kernel")
{
    struct utsname un;
    if (uname(&un) == 0)
    {
        return (strstr(un.release, "Microsoft") != nullptr || strstr(un.release, "WSL") != nullptr
            || strstr(un.version, "Microsoft") != nullptr || strstr(un.version, "WSL") != nullptr);
    }
    else
    {
        return false;
    }
}


#if defined(__cplusplus)
extern "C" {
#endif

bool wiMemoryPopulateInfo(wiMemoryInfo* info)
{
    if (!info)
        return false;
    memset(info, '\0', sizeof(wiMemoryInfo));

    // Annoyingly sysinfo is only useful in a very limited way - it gives free RAM not available
    // So... parse /proc/meminfo :(
    auto mi = get_meminfo_entries();
    bool is_wsl = check_if_wsl();
    if (mi.count("MemTotal") && mi.count("MemAvailable"))
    {
        info->phys.limit = info->phys.total = mi["MemTotal"];
        info->phys.available = mi["MemAvailable"];
        info->phys.used = info->phys.total - info->phys.available;
        info->phys.is_supported = true;
    }
    else if (mi.count("MemTotal") && mi.count("MemFree") && mi.count("Buffers")
        && mi.count("Cached") && mi.count("SReclaimable") && mi.count("Shmem"))
    {
        // If we don't have MemAvailable (e.g. old kernel, WSL) try to approximate
        info->phys.limit = info->phys.total = mi["MemTotal"];
        info->phys.available = mi["MemFree"] + mi["Buffers"] + mi["Cached"] + mi["SReclaimable"] - mi["Shmem"];
        info->phys.used = info->phys.total - info->phys.available;
        info->phys.is_supported = true;
    }

    if (mi.count("SwapTotal") && mi.count("SwapFree"))
    {
        info->swap.limit = info->swap.total = mi["SwapTotal"];
        info->swap.available = mi["SwapFree"];
        info->swap.used = info->swap.total - info->swap.available;
        info->swap.is_supported = true;
    }
    // WSL lies about these values, so don't claim to support them if we're on it
    if (!is_wsl && mi.count("CommitLimit") && mi.count("Committed_AS"))
    {
        info->virt.limit = info->virt.total = mi["CommitLimit"];
        info->virt.used = mi["Committed_AS"];
        info->virt.available = info->virt.total - info->virt.used;
        info->virt.is_supported = true;
    }
    info->is_supported = (info->phys.is_supported || info->swap.is_supported || info->virt.is_supported);
    return info->is_supported;
}

#if defined(__cplusplus)
}
#endif

#endif
