#pragma once

#include "watisit/common.h"

#include <cstdint>
#include <cstdlib>

#include <string>
#include <algorithm>
#include <cctype>

inline bool is_reg_bit_set(uint32_t reg, uint32_t bit) { return ((reg & (1U << bit)) != 0); }

int cstrcopy(char* dest, size_t destsz, const char* src, size_t count);

bool read_file(const char* filename, std::string& output);

// Trim from start (in place)
inline void ltrim(std::string &s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) { return !std::isspace(ch); }));
}

// Trim from end (in place)
inline void rtrim(std::string &s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) { return !std::isspace(ch); }).base(), s.end());
}

// Trim from both ends (in place)
inline void trim(std::string &s)
{
    ltrim(s);
    rtrim(s);
}
