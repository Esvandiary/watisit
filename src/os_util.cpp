#include "watisit/common.h"

#include "os_util.hpp"
#include "util.hpp"

#include <fstream>
#include <cstdlib>
#include <string>
#include <cstring>

#if defined(WATISIT_USE_REGEX)
    #include <regex>

    static const std::regex generic_version_regex(R"___(^(\d+)(?:\.(\d+)(?:\.(\d+)(?:\.(\d+))?)?)?.*$)___");
    static const std::regex unix_version_regex(R"___(^(\d+)\.(\d+)(?:\.(\d+)(?:-(\d+)(?:-.+)?|-.+)?)?$)___");
#endif

bool os_get_version_from_string(wiOSComponentInfo& cinfo, const char* verstr, bool write_str)
{
    std::string vs;
#if defined(WATISIT_USE_REGEX)
    std::cmatch match;
    if (std::regex_match(verstr, match, generic_version_regex))
    {
        // Only take as many numbers as we actually found
        cinfo.version_major = static_cast<uint32_t>(std::atoi(match[1].str().c_str()));
        vs += std::to_string(cinfo.version_major);
        if (match.size() > 2)
        {
            cinfo.version_minor = static_cast<uint32_t>(std::atoi(match[2].str().c_str()));
            vs += std::string(".") + std::to_string(cinfo.version_minor);
        }
        if (match.size() > 3)
        {
            cinfo.version_patch = static_cast<uint32_t>(std::atoi(match[3].str().c_str()));
            vs += std::string(".") + std::to_string(cinfo.version_patch);
        }
        if (match.size() > 4)
        {
            cinfo.version_build = static_cast<uint32_t>(std::atoi(match[4].str().c_str()));
            vs += std::string(".") + std::to_string(cinfo.version_build);
        }
        if (write_str)
            cstrcopy(cinfo.version_string, WATISIT_OS_VERSION_STRING_LENGTH, vs.c_str(), vs.size());
        return true;
    }
    else
    {
        return false;
    }
#else
    {
        std::string line(verstr);
        // Find first dot, ensure we have numbers (and only numbers) before it
        const size_t nnum1 = line.find_first_not_of("0123456789");
        if (line.size() == 0 || nnum1 == 0) return false;
        // Store major version, then stop here if we don't have a dot, or remove major version part of string
        cinfo.version_major = static_cast<uint32_t>(std::atoi(line.substr(0, nnum1).c_str()));
        vs += std::to_string(cinfo.version_major);
        if (nnum1 == std::string::npos || line[nnum1] != '.') goto finish;
        line = line.substr(nnum1 + 1);
        // Find new first dot, ensure we have numbers (and only numbers) before it
        const size_t nnum2 = line.find_first_not_of("0123456789");
        if (line.size() == 0 || nnum2 == 0) goto finish;
        // Store minor version, then stop here if we don't have a dot, or remove minor version part of string
        cinfo.version_minor = static_cast<uint32_t>(std::atoi(line.substr(0, nnum2).c_str()));
        vs += std::string(".") + std::to_string(cinfo.version_minor);
        if (nnum2 == std::string::npos || line[nnum2] != '.') goto finish;
        line = line.substr(nnum2 + 1);
        // Find next non-number element (or end of string), ensure we have numbers before it
        const size_t nnum3 = line.find_first_not_of("0123456789");
        if (line.size() == 0 || nnum3 == 0) goto finish;
        // Store patch version, then stop here if we don't have a dot, or remove patch version part of string
        cinfo.version_patch = static_cast<uint32_t>(std::atoi(line.substr(0, nnum3).c_str()));
        vs += std::string(".") + std::to_string(cinfo.version_patch);
        if (nnum3 == std::string::npos || line[nnum3] != '.') goto finish;
        // Find next non-number element (or end of string), ensure we have numbers before it
        const size_t nnum4 = line.find_first_not_of("0123456789");
        if (line.size() == 0 || nnum4 == 0) goto finish;
        // Store build version
        cinfo.version_build = static_cast<uint32_t>(std::atoi(line.substr(0, nnum4).c_str()));
        vs += std::string(".") + std::to_string(cinfo.version_build);
    }
finish:
    // Store original string into version_string
    if (write_str)
        cstrcopy(cinfo.version_string, WATISIT_OS_VERSION_STRING_LENGTH, vs.c_str(), vs.size());
    return true;
#endif
}


#if defined(WATISIT_OS_UNIX)

static const char* arch64list[] = {
    "x86_64",
    "amd64",
    "arm64",
    "aarch64",
    nullptr
    // TODO: Need to update for other architectures?
};

#if defined(WATISIT_USE_REGEX)
static std::regex vardef_line_regex(R"___(^(\w+)\s*=\s*["]?([^"]*)["]?$)___");
#endif

std::map<std::string,std::string> parse_vardef_file(const char* filename)
{
    std::map<std::string,std::string> output;
    std::ifstream fs(filename);
    if (fs.good())
    {
        std::string line;
        while (std::getline(fs, line))
        {
#if defined(WATISIT_USE_REGEX)
            std::smatch match;
            if (!std::regex_match(line, match, vardef_line_regex)) continue;
            output[match[1].str()] = match[2].str();
#else
            trim(line);
            size_t equals = line.find('=');
            if (equals == std::string::npos) continue;
            std::string name = line.substr(0, equals);
            std::string value = line.substr(equals+1);
            if (value.size() > 0 && value.front() == '"' && value.back() == '"')
                value = value.substr(1, value.size() - 2);
            output[name] = value;
#endif
        }
    }
    return output;
}


bool os_unix_get_kernel_info(wiOSInfo& info, const struct utsname& un)
{
    // Kernel name
    cstrcopy(info.kernel.name, WATISIT_OS_SHORT_NAME_LENGTH, un.sysname, std::strlen(un.sysname));

    // Detect 32/64-bit OS (hacky)
    info.word_size = 32;
    for (size_t i = 0; arch64list[i] != nullptr; ++i)
    {
        if (std::strcmp(un.machine, arch64list[i]) == 0)
        {
            info.word_size = 64;
            break;
        }
    }

    return true;
}

bool os_unix_get_kernel_version(wiOSComponentInfo& cinfo, const struct utsname& un)
{
#if defined(WATISIT_USE_REGEX)
    // Kernel version numbers and string
    std::cmatch match;
    if (std::regex_match(un.release, match, unix_version_regex))
    {
        cinfo.version_major = static_cast<uint32_t>(std::stoi(match[1].str()));
        cinfo.version_minor = static_cast<uint32_t>(std::stoi(match[2].str()));
        cinfo.version_patch = match.size() > 3 ? static_cast<uint32_t>(std::stoi(match[3].str())) : 0;
        cstrcopy(cinfo.version_string, WATISIT_OS_VERSION_STRING_LENGTH, un.release, sizeof(un.release));
        return true;
    }
    else
    {
        return false;
    }
#else
    std::string line(un.release);
    // Find first dot, ensure we have numbers (and only numbers) before it
    const size_t dot1 = line.find('.');
    if (line.find_first_not_of("0123456789") != dot1) return false;
    // Store major version and remove major version part of string
    const uint32_t ver_major = static_cast<uint32_t>(std::stoi(line.substr(0, dot1)));
    line = line.substr(dot1 + 1);
    // Find new first dot, ensure we have numbers (and only numbers) before it
    const size_t dot2 = line.find('.');
    if (line.find_first_not_of("0123456789") != dot2) return false;
    // Store minor version and remove minor version part of string
    const uint32_t ver_minor = static_cast<uint32_t>(std::stoi(line.substr(0, dot2)));
    line = line.substr(dot2 + 1);
    // Find next non-number element (or end of string), ensure we have numbers before it
    const size_t nextelem = line.find_first_not_of("0123456789");
    if (nextelem == 0) return false;
    // Store patch version and decide what to do next
    const uint32_t ver_patch = static_cast<uint32_t>(std::stoi(line.substr(0, nextelem)));
    // Write to output variable
    cinfo.version_major = ver_major;
    cinfo.version_minor = ver_minor;
    cinfo.version_patch = ver_patch;
    // If we are not at end-of-string and we have a dash...
    if (nextelem != std::string::npos && line[nextelem] == '-')
    {
        line = line.substr(nextelem + 1);
        const size_t nextdash = line.find_first_not_of("0123456789");
        // If we have characters left and they start with numbers...
        if (line.size() > 0 && (nextdash != 0))
            cinfo.version_build = static_cast<uint32_t>(std::stoi(line.substr(0, nextdash)));
    }
    cstrcopy(cinfo.version_string, WATISIT_OS_VERSION_STRING_LENGTH, un.release, sizeof(un.release));
    return true;
#endif
}

#endif
