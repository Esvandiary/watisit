#include "watisit/common.h"

#if defined(WATISIT_ARCH_ARM)

#include "watisit/cpu.h"
#include "util.hpp"

#include <unordered_map>

#include <string.h>
#include <stdint.h>

#if defined(WATISIT_OS_LINUX)
    #include <sys/sysinfo.h>
    #include <unistd.h>
    #include <sys/auxv.h>
    #include <asm/hwcap.h>
#elif defined(WATISIT_OS_WINDOWS)
    #include <windows.h>
#endif

#if defined(WATISIT_OS_WINDOWS)
static bool cpu_arm_windows_get_info_from_registry(wiCPUInfo& info)
{
    HKEY reg_nthcp;
    const char* subkey = "HARDWARE\\DESCRIPTION\\System\\CentralProcessor";
    if (::RegOpenKeyExA(HKEY_LOCAL_MACHINE, subkey, 0, KEY_READ, &reg_nthcp) == ERROR_SUCCESS)
    {
        std::unordered_map<std::string, uint32_t> coreCount;
        std::unordered_map<std::string, std::string> vendorMap;
        char buf[256];
        DWORD buflen;
        for (DWORD i = 0; buflen = sizeof(buf), ::RegEnumKeyExA(reg_nthcp, i, buf, &buflen, nullptr, nullptr, nullptr, nullptr) == ERROR_SUCCESS; ++i)
        {
            HKEY reg_curproc;
            if (::RegOpenKeyExA(reg_nthcp, buf, 0, KEY_READ, &reg_curproc) == ERROR_SUCCESS)
            {
                BYTE valbuf[256];
                DWORD valbuflen = sizeof(valbuf);
                std::string name, vendor;
                if (::RegQueryValueExA(reg_curproc, "ProcessorNameString", nullptr, nullptr, valbuf, &valbuflen) == ERROR_SUCCESS)
                    name = std::string(reinterpret_cast<char*>(valbuf));
                if (::RegQueryValueExA(reg_curproc, "VendorIdentifier", nullptr, nullptr, valbuf, &valbuflen) == ERROR_SUCCESS)
                    vendor = std::string(reinterpret_cast<char*>(valbuf));

                vendorMap[name] = vendor;
                auto cit = coreCount.find(name);
                if (cit != coreCount.end())
                    ++cit->second;
                else
                    coreCount[name] = 1;

                ::RegCloseKey(reg_curproc);
            }
        }

        if (coreCount.size() == 1 && vendorMap.size() == 1)
        {
            cstrcopy(info.description, WATISIT_CPU_DESCRIPTION_SIZE, coreCount.begin()->first.c_str(), coreCount.begin()->first.size());
            cstrcopy(info.vendor_name, WATISIT_CPU_VENDOR_NAME_SIZE, vendorMap.begin()->second.c_str(), vendorMap.begin()->second.size());
            info.num_physical_cores = coreCount.begin()->second;
            info.num_logical_cores = wiGetTotalLogicalCPUCount(); // TODO: better?
        }

        ::RegCloseKey(reg_nthcp);
        return (coreCount.size() == 1 && vendorMap.size() == 1);
    }
    return false;
}

using FN_ISWOW64PROCESS = BOOL(WINAPI*)(HANDLE, PBOOL);
static const FN_ISWOW64PROCESS fnIsWow64 = (FN_ISWOW64PROCESS)::GetProcAddress(::GetModuleHandleA("kernel32"), "IsWow64Process");
using FN_ISWOW64PROCESS2 = BOOL(WINAPI*)(HANDLE, PUSHORT, PUSHORT);
static const FN_ISWOW64PROCESS2 fnIsWow64P2 = (FN_ISWOW64PROCESS2)::GetProcAddress(::GetModuleHandleA("kernel32"), "IsWow64Process2");

static uint32_t cpu_arm_windows_get_word_size(void)
{
#if defined(WATISIT_ARCHBITS) && WATISIT_ARCHBITS == 64
    // We're running in 64-bit mode...
    return 64;
#else
    if (fnIsWow64P2)
    {
        USHORT wow = 0, native = 0;
        if (fnIsWow64P2(::GetCurrentProcess(), &wow, &native))
            return (native == IMAGE_FILE_MACHINE_ARM64) ? 64 : 32;
    }
    if (fnIsWow64)
    {
        BOOL isWow64 = FALSE;
        if (fnIsWow64(::GetCurrentProcess(), &isWow64))
            return (isWow64 ? 64 : 32);
        else
            return 0;
    }
    else
    {
        // Ancient Windows, so 32-bit
        return 32;
    }
#endif
}
#endif

static bool cpu_arm_has_neon()
{
#if defined(WATISIT_ARCH_ARM64)
    return true; // NEON is mandatory for AArch64
#elif defined(WATISIT_OS_LINUX) || defined(WATISIT_OS_ANDROID)
    return (getauxval(AT_HWCAP) & HWCAP_NEON);
#elif defined(WATISIT_OS_WINDOWS)
    return true; // WoA baseline feature set includes NEON
#else
    #error "No cpu_arm_has_neon implementation for this platform"
#endif
}

#if defined(__cplusplus)
extern "C" {
#endif

bool wiCPUPopulateInfo(wiCPUInfo* info)
{
    if (!info)
        return false;
    memset(info, '\0', sizeof(wiCPUInfo));

#if defined(WATISIT_OS_WINDOWS)
    cpu_arm_windows_get_info_from_registry(*info);
    info->word_size = cpu_arm_windows_get_word_size();
#endif

    info->features.NEON = cpu_arm_has_neon();

    info->is_supported = true;
    return info->is_supported;
}

#if defined(__cplusplus)
}
#endif

#endif
