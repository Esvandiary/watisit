#include "watisit/common.h"

#if defined(WATISIT_OS_MACOS)

#include <cstring>

#include "watisit/os.h"
#include "os_util.hpp"
#include "util.hpp"

#include <sys/utsname.h>
#include <tinyxml2.h>

static const char* os_name = "macOS";

static bool os_macos_get_kernel_info(wiOSInfo& info)
{
    struct utsname un;
    if (uname(&un) == 0)
    {
        // Kernel name, version
        bool iok = os_unix_get_kernel_info(info, un);
        bool vok = os_get_version_from_string(info.kernel, un.release, true);
        info.kernel.is_supported = (iok || vok);
    }
    return info.kernel.is_supported;
}


static bool os_macos_read_basic_plist(const char* filename, std::map<std::string,std::string>& output)
{
    tinyxml2::XMLDocument doc;
    if (doc.LoadFile(filename) != tinyxml2::XML_SUCCESS) return false;
    const tinyxml2::XMLElement* root = doc.RootElement();
    if (root == nullptr) return false;
    const tinyxml2::XMLElement* dict = root->FirstChildElement("dict");
    if (dict == nullptr) return false;
    const tinyxml2::XMLElement* current_key = dict->FirstChildElement("key");
    while (current_key != nullptr)
    {
        std::string key_name = current_key->GetText();
        const tinyxml2::XMLElement* current_value = current_key->NextSiblingElement();
        if (current_value != nullptr && strcmp(current_value->Name(), "string") == 0)
        {
            output[key_name] = std::string(current_value->GetText());
        }
        current_key = current_key->NextSiblingElement("key");
    }
    return true;
}


static bool os_macos_get_distro_info(OSInfo& info)
{
    std::map<std::string,std::string> plist_info;
    bool loaded_plist = os_macos_read_basic_plist("/System/Library/CoreServices/SystemVersion.plist", plist_info);
    loaded_plist = loaded_plist || os_macos_read_basic_plist("/System/Library/CoreServices/ServerVersion.plist", plist_info);
    std::map<std::string,std::string>::const_iterator name_it, ver_it;
    if (loaded_plist)
    {
        auto name_it = plist_info.find("ProductName");
        auto ver_it = plist_info.find("ProductUserVisibleVersion");
        if (ver_it == plist_info.end())
            ver_it = plist_info.find("ProductVersion");

        if (name_it != plist_info.end())
            cstrcopy(info.distro.name, WATISIT_OS_COMPONENT_NAME_LENGTH, name_it->second.c_str(), name_it->second.size());
        if (ver_it != plist_info.end())
        {
            os_get_version_from_string(info.distro, ver_it->second.c_str(), true);
            info.distro.is_supported = true;
        }

        if (name_it != plist_info.end() && ver_it != plist_info.end())
        {
            std::string desc = name_it->second + std::string(" ") + ver_it->second;
            cstrcopy(info.description, WATISIT_OS_DESCRIPTION_LENGTH, desc.c_str(), desc.size());
            return true;
        }
    }
    return false;
}


#if defined(__cplusplus)
extern "C" {
#endif

bool wiOSPopulateInfo(wiOSInfo* info)
{
    if (!info)
        return false;
    memset(info, '\0', sizeof(wiOSInfo));

    info->os = wiOS_MacOS;
    cstrcopy(info.name, WATISIT_OS_SHORT_NAME_LENGTH, os_name, std::strlen(os_name));

    os_macos_get_kernel_info(*info);
    os_macos_get_distro_info(*info);

    info->is_supported = true;
    return info->is_supported;
}

#if defined(__cplusplus)
}
#endif

#endif // OS
