#include "watisit/common.h"

#if defined(WATISIT_OS_BSD)

#include "watisit/memory.h"
#include "util.hpp"

#include <sys/types.h>
#include <sys/sysctl.h>


#if defined(__cplusplus)
extern "C" {
#endif

bool wiMemoryPopulateInfo(wiMemoryInfo* info)
{
    // TODO: Use sysctl for phys mem and kvm for swap (virt?)
    #warning "TODO: BSD get_memory_info_os implementation"

    if (!info)
        return false;
    memset(info, '\0', sizeof(wiMemoryInfo));
    return false;
}

#if defined(__cplusplus)
}
#endif

#endif