#include "watisit/common.h"

#if defined(WATISIT_OS_UNKNOWN)

#include "watisit/os.h"
#include "os_util.hpp"


#if defined(__cplusplus)
extern "C" {
#endif

bool wiOSPopulateInfo(wiOSInfo* info)
{
    if (!info)
        return false;
    memset(info, '\0', sizeof(wiOSInfo));
    return false;
}

#if defined(__cplusplus)
}
#endif

#endif // OS