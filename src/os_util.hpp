#pragma once

#include "watisit/common.h"
#include "watisit/os.h"

#include <map>
#include <string>

#if defined(WATISIT_OS_UNIX)
// This is correct on Linux and BSD at least
#include <sys/utsname.h>
#endif

bool os_get_version_from_string(wiOSComponentInfo& cinfo, const char* verstr, bool write_str);

#if defined(WATISIT_OS_UNIX)
bool os_unix_get_kernel_info(wiOSInfo& info, const struct utsname& un);
bool os_unix_get_kernel_version(wiOSComponentInfo& cinfo, const struct utsname& un);
std::map<std::string,std::string> parse_vardef_file(const char* filename);
#endif