#include "watisit/common.h"

#include "util.hpp"

#include <fstream>

int cstrcopy(char* dest, size_t destsz, const char* src, size_t count)
{
    if (!count) return 0;
    if (!dest || !src || !destsz) return -22; // EINVAL
    size_t i;
    size_t end = ((count < destsz) ? count : destsz-1);
    for (i = 0; i < end && src[i]; ++i)
        dest[i] = src[i];
    dest[i] = '\0';
    return 0;
}


bool read_file(const char* filename, std::string& output)
{
    std::ifstream ifs(filename);
    if (ifs.good())
    {
        ifs.seekg(0, std::ios::end);
        std::streampos size = ifs.tellg();
        ifs.seekg(0, std::ios::beg);

        output.clear();
        output.reserve(size);
        ifs.read(&output[0], size);
        return true;
    }
    else
    {
        return false;
    }
}
